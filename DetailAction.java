package admin.user;

import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import bean.User;
import dao.UserDAO;
import tool.Action;
/**
 * @author 福永
 * Idをもとに社員情報・職務経歴を取得する
 */
public class DetailAction extends Action{

	public String execute(
			HttpServletRequest request, HttpServletResponse response
	) throws Exception {

		String id =request.getParameter("id");
		UserDAO dao=new UserDAO();
		HashMap<String, String> whereInfo = new HashMap<String, String>();
		whereInfo.put("us.id",id);
		HashMap<String, String> where = new HashMap<String, String>();
		where.put("userId", id);

		//資格以外の社員情報を取得
		List<User> userInfo = dao.dbGetData("select * from tbl_user as us JOIN tbl_department as dp ON us.department = dp.id ", whereInfo, "user");
		//資格情報を取得
		List<User> listCertificate = dao.dbGetData("select * from tbl_certificate_detail as dt INNER JOIN tbl_certificate as c ON dt.certificateId = c.id", where, "listCertificate");
		//職務経歴を取得
		List<User> listWorkHistory = dao.dbGetData("select * from tbl_work_history as wh join tbl_industry as i on wh.industryId = i.id", where, "workHistory");

		for(User item : listWorkHistory) {
			String workHistoryId = Integer.toString(item.getWorkHistoryId());
			HashMap<String, String> whereWorkHistory = new HashMap<String, String>();
			whereWorkHistory.put("workHistoryId", workHistoryId);
			//workHistoryIdごとにlanguageNameを取得
			item.setListLanguage( dao.dbGetData("select languageName from tbl_work_history_detail as hd join tbl_language as la on hd.languageId = la.id", whereWorkHistory, "listLanguage"));
			//workHistoryIdごとにdatabaseNameを取得
			item.setListDatabase( dao.dbGetData("select databaseName from tbl_work_history_detail as hd join tbl_database as db on hd.databaseId = db.id", whereWorkHistory, "listDatabase"));
			//workHistoryIdごとにframeworkNameを取得
			item.setListFramework( dao.dbGetData("select frameworkName from tbl_work_history_detail as hd join tbl_framework as fw on hd.frameworkId = fw.id", whereWorkHistory, "listFramework"));

		}

		for(User item : userInfo) {
			item.setWorkHistoryInsertUrl("WorkHistoryInsert.action?id=" + id);
			item.setEditInfoUrl("Edit.action?id=" + id);
		}

		request.setAttribute("userInfo", userInfo);
		request.setAttribute("listCertificate", listCertificate);
		request.setAttribute("listWorkHistory", listWorkHistory);


		return "../user/detail.jsp";
	}
}

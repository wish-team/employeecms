package admin.manager;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import bean.Error;
import bean.Helper;
import dao.ManagerDAO;
import tool.Action;

public class AddFrameworkAction extends Action {
	//	@SuppressWarnings("unchecked")
	public String execute(HttpServletRequest request, HttpServletResponse response) throws Exception {

		HashMap<String, String> map = new HashMap<String, String>();
		Error er = new Error();
		Helper help = new Helper();
		String id = request.getParameter("id");
		String frameworkName = request.getParameter("frameworkName");

		if (id.isEmpty()) {
			er.setError("IDを入力してください");
			request.setAttribute("error", er);
			return "addFramework.jsp";
		}

		if (frameworkName.isEmpty()) {
			er.setError("フレームワーク名を入力してください");
			request.setAttribute("error", er);
			return "addFramework.jsp";
		}

		if (!help.isNormalText(id)) {
			er.setError("半角で入力してください");
			request.setAttribute("error", er);
			return "addFramework.jsp";
		}

		map.put("id", id);
		map.put("frameworkName", frameworkName);

		ManagerDAO dao = new ManagerDAO();

		/*
		 *ID・FW名の検索と比較
		 */

		if (dao.isExist("tbl_framework", "id", id)) {
			er.setError("すでに存在しているIDです。");
			request.setAttribute("error", er);
			return "addFramework.jsp";
		}

		if (dao.isExist("tbl_framework", "frameworkName", frameworkName)) {
			er.setError("すでに存在しているFW名です。");
			request.setAttribute("error", er);
			return "addFramework.jsp";
		}

		dao.dbInsert("tbl_framework", map, 2);

		return "showMaster.jsp";
	}
}

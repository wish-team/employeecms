package user;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import bean.Error;
import bean.User;
import dao.UserDAO;
import tool.Action;

/*
 * 技術者ログイン
 * abe yasuko
 */
public class LoginAction extends Action{

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		//セッションの開始
		HttpSession session = request.getSession();
		Error er = new Error();

		//パラメータの取得
		String username = request.getParameter("username");
		String password = request.getParameter("password");

		//入力値がからの場合の判定処理
		if(username.isEmpty() || password.isEmpty()) {
			er.setError("IDとPASSを入力してください。");
			request.setAttribute("error", er);
			return "login.jsp";
		}

		//一致したログインIDがあった場合の判定処理



		//取得したパラメートをDAOのsearchメソッドを使用して検索
		UserDAO dao = new UserDAO();
		User user = dao.login(username,password);

		//ログイン成功の処理
		if(user != null) {
			session.setAttribute("user", user);
			//職務経歴確認画面に遷移
			return "workHistry.jsp";
		}

		//ログイン失敗の処理
		return "";
	}

}

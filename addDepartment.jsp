<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@include file="/header.jsp"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<div id="content">

	<div class="add-department">
		<form action="AddDepartment.action" method="post" id="add-department-form">

	<div id="department-input">
				<input type="text" name="departmentName" placeholder="部署名" id="department-id">
				<input type="text" name="director" placeholder="部長" id="department-name">
			</div>

			<c:choose>
				<c:when test="${error != null}">
						${error.getError()}
					</c:when>
			</c:choose>

			<div class="insert-button">
				<input type="submit" value="追加" class="form-button">
				<a href="../manager/Master.action?table=6" class="form-button">キャンセル</a>
			</div>
		</form>
	</div>
</div>
<%@include file="/footer.jsp"%>
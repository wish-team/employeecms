<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@include file="/header.jsp"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<div id="content">

	<div class="add-certificate">
		<form action="AddCertificate.action" method="post" id="add-certificate-form">

				<input type="text" name="certificateName" placeholder="資格名" id="certificate-name">

				<c:choose>
					<c:when  test="${error != null}">
						${error.getError()}
					</c:when>
				</c:choose>

			<div class="insert-button">
				<input type="submit" value="追加" class="form-button">
				<a href="../manager/Master.action?table=5" class="form-button">キャンセル</a>
			</div>

		</form>
	</div>
</div>

<%@include file="/footer.jsp"%>
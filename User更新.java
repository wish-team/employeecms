package bean;

import java.util.Date;
import java.util.List;

public class User更新 implements java.io.Serializable {

	private int languageId;
	private int databaseId;
	private int frameworkId;
	private int industryId;
	private int workHistoryId;
	private int departmentId;
	private int userId;
	private String EmployeeEditor;
	private String EmployeeEditDate;
	private String detailUrl;
	private String editUrl;
	private String deleteUrl;
	private List<User更新> listCertificate;
	private String workHistoryInsertUrl;
	private String editInfoUrl;
	private String editUserWorkHistoryUrl;
	private String editUserInfoUrl;

	public String getEditUserInfoUrl() {
		return editUserInfoUrl;
	}
	public void setEditUserInfoUrl(String editUserInfoUrl) {
		this.editUserInfoUrl = editUserInfoUrl;
	}
	public String getEditInfoUrl() {
		return editInfoUrl;
	}
	public void setEditInfoUrl(String editInfoUrl) {
		this.editInfoUrl = editInfoUrl;
	}
	public String getEditUserWorkHistoryUrl() {
		return editUserWorkHistoryUrl;
	}
	public void setEditUserWorkHistoryUrl(String editUserWorkHistoryUrl) {
		this.editUserWorkHistoryUrl = editUserWorkHistoryUrl;
	}
	public String getWorkHistoryInsertUrl() {
		return workHistoryInsertUrl;
	}
	public void setWorkHistoryInsertUrl(String workHistoryInsertUrl) {
		this.workHistoryInsertUrl = workHistoryInsertUrl;
	}
	public List<User更新> getListCertificate() {
		return listCertificate;
	}
	public void setListCertificate(List<User更新> listCertificate) {
		this.listCertificate = listCertificate;
	}
	public String getDetailUrl() {
		return detailUrl;
	}
	public void setDetailUrl(String detailUrl) {
		this.detailUrl = detailUrl;
	}
	public String getEditUrl() {
		return editUrl;
	}
	public void setEditUrl(String editUrl) {
		this.editUrl = editUrl;
	}
	public String getDeleteUrl() {
		return deleteUrl;
	}
	public void setDeleteUrl(String deleteUrl) {
		this.deleteUrl = deleteUrl;
	}
	public String getEmployeeEditor() {
		return EmployeeEditor;
	}
	public void setEmployeeEditor(String employeeEditor) {
		EmployeeEditor = employeeEditor;
	}
	public String getEmployeeEditDate() {
		return EmployeeEditDate;
	}
	public void setEmployeeEditDate(String employeeEditDate) {
		EmployeeEditDate = employeeEditDate;
	}
	public int getLanguageId() {
		return languageId;
	}
	public void setLanguageId(int languageId) {
		this.languageId = languageId;
	}
	public int getDatabaseId() {
		return databaseId;
	}
	public void setDatabaseId(int databaseId) {
		this.databaseId = databaseId;
	}
	public int getFrameworkId() {
		return frameworkId;
	}
	public void setFrameworkId(int frameworkId) {
		this.frameworkId = frameworkId;
	}
	public int getIndustryId() {
		return industryId;
	}
	public void setIndustryId(int industryId) {
		this.industryId = industryId;
	}
	public int getWorkHistoryId() {
		return workHistoryId;
	}
	public void setWorkHistoryId(int workHistoryId) {
		this.workHistoryId = workHistoryId;
	}
	public int getDepartmentId() {
		return departmentId;
	}
	public void setDepartmentId(int departmentId) {
		this.departmentId = departmentId;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getFullname() {
		return fullname;
	}
	public void setFullname(String fullname) {
		this.fullname = fullname;
	}
	public int getGender() {
		return gender;
	}
	public void setGender(int gender) {
		this.gender = gender;
	}
	public String getBirth() {
		return birth;
	}
	public void setBirth(String birth) {
		this.birth = birth;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public int getExperience() {
		return experience;
	}
	public void setExperience(int experience) {
		this.experience = experience;
	}
	public String getDepartmentName() {
		return departmentName;
	}
	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public String getCreator() {
		return creator;
	}
	public void setCreator(String creator) {
		this.creator = creator;
	}
	public String getCreateDate() {
		return createDate;
	}
	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}
	public String getEditor() {
		return editor;
	}
	public void setEditor(String editor) {
		this.editor = editor;
	}
	public String getEditDate() {
		return editDate;
	}
	public void setEditDate(String editDate) {
		this.editDate = editDate;
	}
	public String getDeleter() {
		return deleter;
	}
	public void setDeleter(String deleter) {
		this.deleter = deleter;
	}
	public String getDeleteDate() {
		return deleteDate;
	}
	public void setDeleteDate(String deleteDate) {
		this.deleteDate = deleteDate;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public int getId() {
		return Id;
	}
	public void setId(int Id) {
		this.Id = Id;
	}
	public String getCertificateName() {
		return certificateName;
	}
	public void setCertificateName(String certificateName) {
		this.certificateName = certificateName;
	}
	public int getCertificateId() {
		return certificateId;
	}
	public void setCertificateId(int certificateId) {
		this.certificateId = certificateId;
	}
	public String getIndustryName() {
		return industryName;
	}
	public void setIndustryName(String industryName) {
		this.industryName = industryName;
	}
	public String getDetail() {
		return detail;
	}
	public void setDetail(String detail) {
		this.detail = detail;
	}
	public String getStartTime() {
		return startTime;
	}
	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}
	public String getEndTime() {
		return endTime;
	}
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}
	public String getLanguageName() {
		return languageName;
	}
	public void setLanguageName(String languageName) {
		this.languageName = languageName;
	}
	public String getDatabaseName() {
		return databaseName;
	}
	public void setDatabaseName(String databaseName) {
		this.databaseName = databaseName;
	}
	public String getFrameworkName() {
		return framworkName;
	}
	public void setFrameworkName(String framworkName) {
		this.framworkName = framworkName;
	}
	private String fullname;
	private int gender;
	private String birth;
	private int age;
	private int experience;
	private String departmentName;
	private int status;
	private String creator;
	private String createDate;
	private String editor;
	private String editDate;
	private String deleter;
	private String deleteDate;
	private String username;
	private String password;
	private int Id;
	private String certificateName;
	private int certificateId;
	private String industryName;
	private String detail;
	private String startTime;
	private String endTime;
	private String languageName;
	private String databaseName;
	private String framworkName;
	private List<User更新> listLanguage;
	private List<User更新> listDatabase;
	private List<User更新> listFramework;

	public List<User更新> getListLanguage() {
		return listLanguage;
	}
	public void setListLanguage(List<User更新> listLanguage) {
		this.listLanguage = listLanguage;
	}
	public List<User更新> getListDatabase() {
		return listDatabase;
	}
	public void setListDatabase(List<User更新> listDatabase) {
		this.listDatabase = listDatabase;
	}
	public List<User更新> getListFramework() {
		return listFramework;
	}
	public void setListFramework(List<User更新> listFramework) {
		this.listFramework = listFramework;
	}
	public void setBirth_date(Date birth2) {
		// TODO 自動生成されたメソッド・スタブ

	}
	public void setCertificate_ids(int[] certificate_ids) {
		// TODO 自動生成されたメソッド・スタブ

	}
	public int[] getCertificate_ids() {
		// TODO 自動生成されたメソッド・スタブ
		return null;
	}
	public Date getBirth_date() {
		// TODO 自動生成されたメソッド・スタブ
		return null;
	}

}

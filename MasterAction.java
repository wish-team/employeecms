package admin.manager;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import bean.Manager;
import bean.Render;
import dao.ManagerDAO;
import tool.Action;
public class MasterAction extends Action {
//	@SuppressWarnings("unchecked")
	public String execute(HttpServletRequest request, HttpServletResponse response) throws Exception {

//		HttpSession session = request.getSession();
		Render rd = new Render();
		List<Manager> list = new ArrayList<Manager>();
		ManagerDAO dao = new ManagerDAO();
		int table = Integer.parseInt(request.getParameter("table"));


		if(table == 1) {
			list = dao.dbGetData("select * from tbl_database", null, "database");
			rd.setDatabaseTable(list);
			request.setAttribute("database", rd);
		}

		if(table == 2) {
			list = dao.dbGetData("select * from tbl_language", null, "language");
			rd.setLanguageTable(list);
			request.setAttribute("language", rd);
		}

		if(table == 3) {
			list = dao.dbGetData("select * from tbl_industry", null, "industry");
			rd.setIndustryTable(list);
			request.setAttribute("industry", rd);
		}

		if(table == 4) {
			list = dao.dbGetData("select * from tbl_framework", null, "framework");
			rd.setFrameworkTable(list);
			request.setAttribute("framework", rd);
		}

		if(table == 5) {
			list = dao.dbGetData("select * from tbl_certificate", null, "certificate");
			rd.setCertificateTable(list);
			request.setAttribute("certificate", rd);
		}

		if(table == 6) {
			list = dao.dbGetData("select * from tbl_department", null, "department");
			rd.setDepartmentTable(list);
			request.setAttribute("department", rd);
		}
		//７はないからエラー処理を書く



		return "showMaster.jsp";
	}


}

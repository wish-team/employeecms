package admin.user;

import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import bean.Manager;
import bean.User;
import dao.ManagerDAO;
import dao.UserDAO;
import tool.Action;

public class EditAction extends Action{
	public String execute(
			HttpServletRequest request, HttpServletResponse response
	) throws Exception {

		String id =request.getParameter("id");
		UserDAO dao=new UserDAO();

		String btn = request.getParameter("btn-submit");
		if(btn == null) btn ="";

		if(!(btn.isEmpty()) || (btn.equals("submit"))) {
			ManagerDAO Mdao = new ManagerDAO();
			UserDAO dao=new UserDAO();

			String fullname = request.getParameter("fullname");
			String department = request.getParameter("department");//
			String birth = request.getParameter("birth");

//			HashMap<String, String> whereInfo = new HashMap<String, String>();
//			whereInfo.put("us.id",id);
//			HashMap<String, String> where = new HashMap<String, String>();
//			where.put("userId", id);
//			List<User> userInfo = dao.dbGetData("select * from tbl_user as us JOIN tbl_department as dp ON us.department = dp.id ", whereInfo, "user");
//			List<User> listCertificate = dao.dbGetData("select * from tbl_certificate_detail as dt INNER JOIN tbl_certificate as c ON dt.certificateId = c.id", where, "listCertificate");
//			List<User> listWorkHistory = dao.dbGetData("select * from tbl_work_history as wh join tbl_industry as i on wh.industryId = i.id", where, "workHistory");
//
//			for(User item : listWorkHistory) {
//				String workHistoryId = Integer.toString(item.getWorkHistoryId());
//				HashMap<String, String> whereWorkHistory = new HashMap<String, String>();
//				whereWorkHistory.put("workHistoryId", workHistoryId);
//				List<User> listLanguage = dao.dbGetData("select languageName from tbl_work_history_detail as hd join tbl_language as la on hd.languageId = la.id", whereWorkHistory, "listLanguage");
//				List<User> listDatabase = dao.dbGetData("select databaseName from tbl_work_history_detail as hd join tbl_database as db on hd.databaseId = db.id", whereWorkHistory, "listDatabase");
//				List<User> listFramework = dao.dbGetData("select frameworkName from tbl_work_history_detail as hd join tbl_framework as fw on hd.frameworkId = fw.id", whereWorkHistory, "listFramework");
//				item.setListLanguage(listLanguage);
//				item.setListDatabase(listDatabase);
//				item.setListFramework(listFramework);
//			}
//
//			for(User item : userInfo) {
//				item.setWorkHistoryInsertUrl("WorkHistoryInsert.action?id=" + id);
//				item.setEditInfoUrl("Edit.action?id=" + id);
//			}
//
//			List<User> Certificate = dao.dbGetData(
//					"select * from tbl_certificate_detail as dt INNER JOIN tbl_certificate as c ON dt.certificateId = c.id",
//					where, "listCertificate");
//
//			int i = 0;
//			for (User item : listCertificate) {
//				if (i == 0) {
//					String certificate1 = item.getCertificateName();
//					request.setAttribute("certificate1", certificate1);
//				}
//				if (i == 1) {
//					String certificate2 = item.getCertificateName();
//					request.setAttribute("certificate2", certificate2);
//				}
//				if (i == 2) {
//					String certificate3 = item.getCertificateName();
//					request.setAttribute("certificate3", certificate3);
//				}
//				i++;
//			}
//
//			request.setAttribute("Certificate", Certificate);
//
//			request.setAttribute("Language", Language = dao.dbGetData("select * from tbl_language", null, "language"));
//
//			request.setAttribute("Database", Database = dao.dbGetData("select * from tbl_database", null, "database"));
//
//			request.setAttribute("Framework", Framework = dao.dbGetData("select * from tbl_framework", null, "framework"));
//
//			request.setAttribute("Industry", Industry = dao.dbGetData("select * from tbl_industry", null, "industry"));
//			request.setAttribute("userInfo", userInfo);
//			request.setAttribute("listCertificate", listCertificate);
//			request.setAttribute("listWorkHistory", listWorkHistory);
//
//
			return "../home/home.jsp";
		}

		HashMap<String, String> whereInfo = new HashMap<String, String>();
		whereInfo.put("us.id",id);
		HashMap<String, String> where = new HashMap<String, String>();
		where.put("userId", id);
		List<User> userInfo = dao.dbGetData("select * from tbl_user as us JOIN tbl_department as dp ON us.department = dp.id ", whereInfo, "user");
		List<User> listCertificate = dao.dbGetData("select * from tbl_certificate_detail as dt INNER JOIN tbl_certificate as c ON dt.certificateId = c.id", where, "listCertificate");
		List<User> listWorkHistory = dao.dbGetData("select * from tbl_work_history as wh join tbl_industry as i on wh.industryId = i.id", where, "workHistory");

		User oneUserInfo = userInfo.get(0);
		oneUserInfo.setEditUserInfoUrl("../user/Edit.action?id=" + id);
		for(User item : listWorkHistory) {
			String workHistoryId = Integer.toString(item.getWorkHistoryId());
			HashMap<String, String> whereWorkHistory = new HashMap<String, String>();
			whereWorkHistory.put("workHistoryId", workHistoryId);

			item.setListLanguage( dao.dbGetData("select languageName from tbl_work_history_detail as hd join tbl_language as la on hd.languageId = la.id", whereWorkHistory, "listLanguage"));
			item.setListDatabase( dao.dbGetData("select databaseName from tbl_work_history_detail as hd join tbl_database as db on hd.databaseId = db.id", whereWorkHistory, "listDatabase"));
			item.setListFramework( dao.dbGetData("select frameworkName from tbl_work_history_detail as hd join tbl_framework as fw on hd.frameworkId = fw.id", whereWorkHistory, "listFramework"));
			item.setEditUserWorkHistoryUrl("../user/EditUserWorkHistory.action?workHistoryId=" + workHistoryId);
		}

		ManagerDAO masterDao = new ManagerDAO();
		List<Manager> listIndustryDb = masterDao.dbGetData("select * from tbl_industry", null, "industry");
		List<Manager> listLangugeDb = masterDao.dbGetData("select * from tbl_language", null, "language");
		List<Manager> listDatabseDb = masterDao.dbGetData("select * from tbl_database", null, "database");
		List<Manager> listFrameworkDb = masterDao.dbGetData("select * from tbl_framework", null, "framework");

		request.setAttribute("userInfo", userInfo);
		request.setAttribute("listCertificate", listCertificate);
		request.setAttribute("listWorkHistory", listWorkHistory);
		request.setAttribute("listIndustryDb", listIndustryDb);
		request.setAttribute("listLangugeDb", listLangugeDb);
		request.setAttribute("listDatabaseDb", listDatabseDb);
		request.setAttribute("listFrameworkDb", listFrameworkDb);
		return "edit.jsp";
	}


}

<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@include file="/header.jsp"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div id="content">

	<div class="add-language">
		<form action="AddLanguage.action" method="post" id="add-language-form">

				<input type="text" name="languageName" placeholder="言語名" id="language-name">

				<c:choose>
					<c:when  test="${error != null}">
						${error.getError()}
					</c:when>
				</c:choose>

			<div class="insert-button">
				<input type="submit" value="追加" class="form-button">
				<a href="../manager/Master.action?table=2" class="form-button">キャンセル</a>
			</div>

		</form>
	</div>
</div>

<%@include file="/footer.jsp"%>
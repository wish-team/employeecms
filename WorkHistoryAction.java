package admin.user;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import bean.Manager;
import bean.User;
import dao.ManagerDAO;
import dao.UserDAO;
import tool.Action;

public class WorkHistoryAction extends Action {
//	@SuppressWarnings("unchecked")
	public String execute(HttpServletRequest request, HttpServletResponse response) throws Exception {

		//HttpSession session = request.getSession();

		//ユーザーのIDを取得
		String id = request.getParameter("id");
		ManagerDAO dao = new ManagerDAO();
		UserDAO Udao = new UserDAO();

		String btn = request.getParameter("btn-submit");
		if(btn == null) btn = "";
		if((!btn.isEmpty()) || btn.equals("submit")) {
//			List<User> list = dao.dbGetData("dfdf", "", "dfdf");
//			for(User item : list) {
//				item.setDetailUrl("../user/Detail.action?userId=" + item.getUserId());
//				item.setEditUrl("../user/Edit.Action?userId=" + item.getUserId());
//				item.setDeleteUrl("../user/Delete.Action?userId=" + item.getUserId());
//			}
		}

		HashMap<String, String> whereUserInfo = new HashMap<String, String>();
		List<User> userInfo = new ArrayList<User>();
		List<User> workHistoryInfo = new ArrayList<User>();

		List<Manager> listLanguage = new ArrayList<Manager>();
		List<Manager> listDatabase = new ArrayList<Manager>();
		List<Manager> listFramework = new ArrayList<Manager>();
		List<Manager>listIndustry = new ArrayList<Manager>();

		whereUserInfo.put("us.id", id);
		userInfo = Udao.dbGetData("select * from TBL_USER as us JOIN TBL_DEPARTMENT as dp ON us.department= dp.id ",whereUserInfo ,"user");
		request.setAttribute("userInfo", userInfo);

		HashMap<String, String> where = new HashMap<String, String>();

		where.put("userid", id);
	//workHistoryInfo = Udao.dbGetData("select * from tbl_work_history ", where, "workhistory");
	//request.setAttribute("workHistoryInfo", workHistoryInfo);

		List<User> listCertificate = Udao.dbGetData("select * from tbl_certificate_detail as dt INNER JOIN tbl_certificate as c ON dt.certificateId = c.id", where, "listCertificate");
		int i = 0;
	    for(User item : listCertificate) {
		if(i == 0) {
				String certificate1 = item.getCertificateName();
				request.setAttribute("certificate1",certificate1);
				i++;
			}
			if(i == 1) {
				String certificate2 = item.getCertificateName();
				request.setAttribute("certificate2",certificate2);
				i++;
			}
			if(i == 2) {
				String certificate3 = item.getCertificateName();
				request.setAttribute("certificate3",certificate3);
				i++;
			}
		}

		request.setAttribute("listCertificate", listCertificate);

		listLanguage = dao.dbGetData("select * from tbl_language",null,"language");
		request.setAttribute("listLanguage", listLanguage);

		listDatabase = dao.dbGetData("select * from tbl_database",null,"database");
		request.setAttribute("listDatabase", listDatabase);

		listFramework = dao.dbGetData("select * from tbl_framework",null,"framework");
		request.setAttribute("listFramework", listFramework);

		listIndustry = dao.dbGetData("select * from tbl_industry",null,"industry");
		request.setAttribute("listIndustry", listIndustry);


		return "../user/workHistoryInsert.jsp";
	}
}

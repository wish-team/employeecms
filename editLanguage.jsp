<%@page contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@include file="/header.jsp"%>
<% String id = request.getParameter("id"); %>
<div id="content">
	<div class="form-wp">
		<form action="UpdateLanguage.action" method="post" class="edit-form">
			<div class="widget clearfix">
				<div class="fill-label fl-left">
					<label for="languageName">言語名:</label>
				</div>

				<div class="fill-info">
					<input type="hidden" value="<%=id %>" name="id">
					<input type="text" name="languageName" id="languageName" value="">

					<c:choose>
						<c:when test="${error != null}">
							${error.getError()}
						</c:when>
					</c:choose>
				</div>


				<div class="fill-control">
					<input type="submit" name="btn-submit" value="完了" class="edit-form-button">
					<a href="../manager/Master.action?table=2" class="edit-form-button">キャンセル</a>
				</div>
			</div>
		</form>
	</div>
</div>

<%@include file="/footer.jsp"%>

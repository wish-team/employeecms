package admin.home;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import bean.Error;
import dao.ManagerDAO;
import tool.Action;


public class AddAdminAction extends Action {
	public String execute(
			HttpServletRequest request, HttpServletResponse response) throws Exception {

		Error er = new Error();

		//パラメーターより値をそれぞれ取得
		String fullname = request.getParameter("fullname");
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		String subpassword = request.getParameter("subpassword");

		//未入力の判別処理
		if (username.isEmpty() || password.isEmpty() || subpassword.isEmpty()) {
			er.setError("未入力、もしくは正しく入力されていない箇所があります。");
			request.setAttribute("error", er);
			return "addAdmin.jsp"; //仮の処理
		}
		//パスワード一致の判別処理
		if (password.equals(subpassword)) {

			//一致の場合登録処理を行う
			HashMap<String, String> map = new HashMap<String, String>();

			map.put("id", null);
			map.put("fullname", fullname);
			map.put("username", username);
			map.put("password", password);
			map.put("subpassword", subpassword);

			ManagerDAO dao = new ManagerDAO();
			dao.dbInsert("tbl_admin", map, 1);
			return "home.jsp";
		}
		//一致してない場合登録画面に戻す
		er.setError("未入力、もしくは正しく入力されていない箇所があります。");
		request.setAttribute("error", er);
		return "addAdmin.jsp"; //仮の処理

	}

}
package bean;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

public class Render implements java.io.Serializable {

	private String htmlStr;

	public String getPagging() {
		return htmlStr;
	}

	public String setPagging(Double numPage, int page, String baseUrl) {
		int pagePrev;
		int pageNext;
		this.htmlStr = "<ul id='list-paging' class='fl-right'>";
		if (page > 1) {
			pagePrev = page - 1;
			this.htmlStr += "<li class='fl-left'><a href='" + baseUrl + "?page=" + pagePrev + "'><</a></li>";
		}
		for (int i = 1; i <= numPage; i++) {
			String active = "";
			if (i == page)
				active = "class ='active'";
			this.htmlStr += "<li class='fl-left'><a href='" + baseUrl + "?page=" + i + "'>" + i + "</a></li>";
		}
		if (page < numPage) {
			pageNext = page + 1;
			this.htmlStr += "<li class='fl-left'><a href='" + baseUrl + "?page=" + pageNext + "'>></a></li>";
		}
		this.htmlStr += "</ul>";
		return this.htmlStr;
	}


	//DBのゲッタ
	public String getDatabaseTable() {
		return htmlStr;
	}

	//言語のゲッタ
	public String getLanguageTable() {
		return htmlStr;
	}

	//業種のゲッタ
	public String getIndustryTable() {
		return htmlStr;
	}

	//FWのゲッタ
	public String getFrameworkTable() {
		return htmlStr;
	}

	//資格のゲッタ
	public String getCertificateTable() {
		return htmlStr;
	}

	//部署のゲッタ
	public String getDepartmentTable() {
		return htmlStr;
	}

	//DBのセッタ
	public String setDatabaseTable(List<Manager> list, HttpServletRequest request) {
		this.htmlStr = "<div class='data-table-wp'><div class='data-table-title'><h3>DB一覧</h3><a href='../manager/addDatabase.jsp'>追加</a></div><div class='data-table-content'><table id='master-table' class='table'>";

		//For で繰り返して、データをとる
		for (Manager item : list) {
			int id = item.getId();
			item.setEditUrl("../manager/editDatabase.jsp?id=" + id);
			request.setAttribute("id", id);
			this.htmlStr += "<tr class='clearfix'><td><span>" + item.getDatabaseName() + "</span><div class='control-panel fl-right'><a href='" + item.getEditUrl() + "'>変更</a></div></td></tr>";
		}

		this.htmlStr += "</table></div></div>";
		return this.htmlStr;
	}

	//言語のセッタ
	public String setLanguageTable(List<Manager> list,HttpServletRequest request) {
		this.htmlStr = "<div class='data-table-wp'><div class='data-table-title'><h3>言語一覧</h3><a href='../manager/addLanguage.jsp'>追加</a></div><div class='data-table-content'><table id='master-table'class='table'>";

		//For で繰り返して、データをとる
		for (Manager item : list) {
			int id = item.getId();
			item.setEditUrl("../manager/editLanguage.jsp?id=" + id);
			request.setAttribute("id", id);
			this.htmlStr += "<tr class='clearfix'><td><span>" + item.getLanguageName() + "</span><div class='control-panel fl-right'><a href='" + item.getEditUrl() + "'>変更</a></div></td></tr>";
		}

		this.htmlStr += "</table></div></div>";
		return this.htmlStr;
	}

	//業種のセッタ
	public String setIndustryTable(List<Manager> list,HttpServletRequest request) {
		this.htmlStr = "<div class='data-table-wp'><div class='data-table-title'><h3>業種一覧</h3><a href='../manager/addIndustry.jsp'>追加</a></div><div class='data-table-content'><table id='master-table'class='table'>";

		//For で繰り返して、データをとる
		for (Manager item : list) {
			String id = item.getIdStr();
			item.setEditUrl("../manager/editIndustry.jsp?id=" + id);
			request.setAttribute("id", id);
			this.htmlStr += "<tr class='clearfix'><td><span>" +item.getIdStr() + " "+item.getIndustryName() + "</span><div class='control-panel fl-right'><a href='" + item.getEditUrl() + "'>変更</a></div></td></tr>";
		}

		this.htmlStr += "</table></div></div>";
		return this.htmlStr;
	}

	//FWのセッタ
	public String setFrameworkTable(List<Manager> list,HttpServletRequest request) {
		this.htmlStr = "<div class='data-table-wp'><div class='data-table-title'><h3>FW一覧</h3><a href='../manager/addFramework.jsp'>追加</a></div><div class='data-table-content'><table id='master-table'class='table'>";

		//For で繰り返して、データをとる
		for (Manager item : list) {
			String id = item.getIdStr();
			item.setEditUrl("../manager/editFramework.jsp?id=" + id);
			request.setAttribute("id", id);
			this.htmlStr += "<tr class='clearfix'><td><span>" +item.getIdStr()+" "+item.getFrameworkName() + "</span><div class='control-panel fl-right'><a href='" + item.getEditUrl() + "'>変更</a></div></td></tr>";
		}

		this.htmlStr += "</table></div></div>";
		return this.htmlStr;
	}

	//資格のセッタ
	public String setCertificateTable(List<Manager> list,HttpServletRequest request) {
		this.htmlStr = "<div class='data-table-wp'><div class='data-table-title'><h3>資格一覧</h3><a href='../manager/addCertificate.jsp'>追加</a></div><div class='data-table-content'><table id='master-table'class='table'>";

		//For で繰り返して、データをとる
		for (Manager item : list) {
			int id = item.getId();
			item.setEditUrl("../manager/editCertificate.jsp?id=" + id);
			request.setAttribute("id", id);
			this.htmlStr += "<tr class='clearfix'><td><span>" +item.getCertificateName() + "</span><div class='control-panel fl-right'><a href='" + item.getEditUrl() + "'>変更</a></div></td></tr>";
		}

		this.htmlStr += "</table></div></div>";
		return this.htmlStr;
	}

	//部署のセッタ
	public String setDepartmentTable(List<Manager> list,HttpServletRequest request) {
		this.htmlStr = "<div class='data-table-wp'><div class='data-table-title'><h3>部署一覧</h3><a href='../manager/addDepartment.jsp'>追加</a></div><div class='data-table-content'><table id='master-table'>";

		//For で繰り返して、データをとる
		for (Manager item : list) {
			int id = item.getId();
			item.setEditUrl("../manager/editDepartment.jsp?id=" + id);
			request.setAttribute("id", id);
			this.htmlStr += "<tr class='clearfix'><td><span>" + item.getDepartmentName() + "</span><div class='control-panel fl-right'><a href='" + item.getEditUrl() + "'>変更</a></div></td></tr>";
		}

		this.htmlStr += "</table></div></div>";
		return this.htmlStr;
	}


}

package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import bean.User;

public class UserDAO確認画面 extends DAO {
	public List<User> dbGetData(String query, HashMap<String, String> where, String option) throws Exception {
		List<User> list = new ArrayList<>();

		Connection con = getConnection();
		PreparedStatement st = null;

		if ((where == null) || (where.isEmpty()))
			st = con.prepareStatement(query);
		if ((where != null)) {
			int trigger = 1;
			query += " where ";
			for(String key : where.keySet()) {
				if (trigger == where.size()) query += key + "=?";
				if (trigger != where.size()) query += key + "=?,";
				trigger++;
			}
			st = con.prepareStatement(query);
			int i= 1;
			for(String key : where.keySet()) {
				st.setString(i,where.get(key));
				i++;
			}
		}



		ResultSet rs = st.executeQuery();
		if (option.equals("user")) {
			while (rs.next()) {
				User user = new User();
				user.setUserId(rs.getInt("id"));
				user.setFullname(rs.getString("fullname"));
				user.setGender(rs.getInt("sex"));
				user.setBirth(rs.getString("birth"));
				user.setAge(rs.getInt("age"));
				user.setExperience(rs.getInt("experience"));
				user.setDepartmentName(rs.getString("departmentName"));
				user.setStatus(rs.getInt("status"));
//				user.setCreator(rs.getString("username"));
//				user.setCreateDate(rs.getString("createDate"));
//				user.setEditor(rs.getString("editor"));
//				user.setEditDate(rs.getString("editDate"));
//				user.setDeleter(rs.getString("deleter"));
//				user.setDeleteDate(rs.getString("deleteDate"));
				user.setUsername(rs.getString("username"));
				user.setPassword(rs.getString("password"));
				user.setEmployeeEditor(rs.getString("employeeEditor"));
				user.setEmployeeEditDate(rs.getString("employeeEditDate"));

				list.add(user);
			}
		}

		if(option.equals("listCertificate")) {
			while(rs.next()) {
				User user = new User();
				user.setCertificateName(rs.getString("certificateName"));
				list.add(user);
			}
		}

		if(option.equals("workHistory")) {
			while(rs.next()) {
				User workHistory = new User();
				workHistory.setWorkHistoryId(rs.getInt("WorkHistoryId"));
				workHistory.setStartTime(rs.getString("startTime"));
				workHistory.setEndTime(rs.getString("endTime"));
				workHistory.setIndustryName(rs.getString("industryName"));
				list.add(workHistory);
			}
		}

		if(option.equals("workHistoryDetail")) {
			while(rs.next()) {
				User workDetail = new User();
				workDetail.setId(rs.getInt("id"));
				workDetail.setWorkHistoryId(rs.getInt("workHistoryId"));
				workDetail.setLanguageName(rs.getString("languageName"));
				workDetail.setDatabaseName(rs.getString("databaseName"));
				workDetail.setFrameworkName(rs.getString("frameworkName"));
				list.add(workDetail);
			}
		}

		if(option.equals("listLanguage")) {
			while(rs.next()) {
				User language = new User();
				language.setLanguageName(rs.getString("languageName"));
				list.add(language);
			}
		}

		if(option.equals("listDatabase")) {
			while(rs.next()) {
				User database = new User();
				database.setDatabaseName(rs.getString("databaseName"));
				list.add(database);
			}
		}

		if(option.equals("listFramework")) {
			while(rs.next()) {
				User framework = new User();
				framework.setFrameworkName(rs.getString("frameworkName"));
				list.add(framework);
			}
		}

		st.close();
		con.close();

		return list;
	}

//	public int dbGetNumRows(String table, HashMap<String, String> where) throws Exception {
//		Connection con = getConnection();
//		PreparedStatement st = null;
//		int result = 1;
//		String query = "SELECT COUNT(*) FROM " + table;
//		if ((where == null) || (where.isEmpty()))
//			st = con.prepareStatement(query);
//
//		if ((where != null)) {
//			query+= " where ";
//			int trigger = 1;
//			for(String key : where.keySet()) {
//				if (trigger == where.size()) query += key + "=?";
//				if (trigger != where.size()) query += key + "=? AND ";
//			}
//
//			int i= 1;
//			for(String key : where.keySet()) {
//				if(key != null) {
//					String value = where.get(key);
//					st.setString(i,where.get(key));
//					//lam toi day
//					i++;
//				}
//			}
//		}
//
//		ResultSet rs = st.executeQuery();
//		while(rs.next()) {
//			result+=1;
//		}
//		return result;
//	}

	//	public List<User> search(HashMap<String, String> searchKey) throws Exception {
	//		List<User> list = new ArrayList<>();
	//
	//		Connection con = getConnection();
	//		PreparedStatement st = null;
	//
	//		String[] listCertificate = new String[3];
	//		int i = 0;
	//		for(String key: searchKey.keySet()) {
	//			if((key.equals("diplomaOp1")) || (key.equals("diplomaOp2"))||(key.equals("diplomaOp3"))) {
	//				listCertificate[i] = searchKey.get(key);
	//				i++;
	//			}
	//		}
	//
	//		// kiem tra exStart vs exEnd co dong thoi xuat hien khong
	//		// kiem tra certificate co khong neu co thi ghep mang
	//		String query = "select * from tbl_user as us JOIN tbl_certificate_detail as cer ON us.id = cer.id where ";
	//		query+=	"( exStart >= ? AND exEnd <= ? ) OR certificate IN(?, ?, ?)";
	//
	//		for (String key : searchKey.keySet()) {
	//			String value = searchKey.get(key);
	//			if(!(value.isEmpty()) && (value != null)) {
	//				if(key.equals("fullname")) query += "fullname like ? OR ";
	//				if(key.equals("age")) query += "( age >= ?1 AND age <= ?9 ) OR ";
	////				if(dong thoi xuat hien) {
	//					// xu ly dong thoi xuat hien
	//				}else {
	////					if((key.equals("exStart"))) query +="exStart >= ? OR ";
	////					if((key.equals("exEnd"))) query +=" exEnd <= ? OR ";
	//				}
	//			}
	//
	//		}

	//
	//		ResultSet rs = st.executeQuery();

	//		if (option.equals("user")) {
	//			while (rs.next()) {
	//				User user = new User();
	//				user.getUserId();
	//				user.getFullname();
	//				user.getGender();
	//				user.getBirth();
	//				user.getAge();
	//				user.getExperience();
	//				user.getDepartmentName();
	//				user.getStatus();
	//				user.getCreator();
	//				user.getCreateDate();
	//				user.getEditor();
	//				user.getEditDate();
	//				user.getDeleter();
	//				user.getDeleteDate();
	//				user.getUsername();
	//				user.getPassword();
	//				user.getEmployeeEditor();
	//				user.getEmployeeEditDate();
	//				list.add(user);
	//			}
	//		}

	//		st.close();
	//		con.close();

	//		return list;
	//	}

	public List<User> search(String fullname, String diplomaOp1, String diplomaOp2, String diplomaOp3, String age,
			String exStart, String exEnd) throws Exception {
		List<User> list = new ArrayList<>();
		ArrayList<String> listItem = new ArrayList<String>();
		ArrayList<String> listField = new ArrayList<String>();
		Connection con = getConnection();
		PreparedStatement st = null;
		String query = "select distinct us.id from (TBL_USER as us JOIN TBL_CERTIFICATE_DETAIL as dt on us.id = dt.userid )"
				+ "join tbl_department as dp on us.department = dp.id where ";
		String subQueryFullname = "";
		String subQueryAge = "";
		String subQueryEx = "";
		String subQueryCer = "";

		if(!fullname.isEmpty()) {
			subQueryFullname += "fullname like ?";
			listItem.add(subQueryFullname);
			listField.add(fullname);
		}
		if(!age.isEmpty()) {
			subQueryAge += "( age >= ? AND age <= ? )";
			listItem.add(subQueryAge);
			listField.add(age + "1");
			listField.add(age + "9");
		}
		if(!(exStart.isEmpty()) && (exEnd.isEmpty())) {
			subQueryEx += "experience >= ?";
			listItem.add(subQueryEx);
			listField.add(exStart);
		}
		if((exStart.isEmpty()) && !(exEnd.isEmpty())) {
			subQueryEx += "experience <= ?";
			listItem.add(subQueryEx);
			listField.add(exEnd);
		}
		if(!(exStart.isEmpty()) && !(exEnd.isEmpty())) {
			subQueryEx += "( experience >= ? AND experience <= ? )";
			listItem.add(subQueryEx);
			listField.add(exStart);
			listField.add(exEnd);
		}

		ArrayList<String> listCertificate = new ArrayList<String>();
		if(!diplomaOp1.isEmpty()) listCertificate.add(diplomaOp1);
		if(!diplomaOp2.isEmpty()) listCertificate.add(diplomaOp2);
		if(!diplomaOp3.isEmpty()) listCertificate.add(diplomaOp3);
		if(listCertificate.size() > 0) {
			subQueryCer +="certificateId IN (";
			String[] listCer = listCertificate.toArray(new String[0]);
			if(listCer.length == 1)  {
				subQueryCer += "?)";
				listItem.add(subQueryCer);
				listField.add(listCer[0]);
			}
			if(listCer.length == 2)  {
				subQueryCer += "?,?)";
				listItem.add(subQueryCer);
				listField.add(listCer[0]);
				listField.add(listCer[1]);
			}
			if(listCer.length == 3)  {
				subQueryCer += "?,?,?)";
				listItem.add(subQueryCer);
				listField.add(listCer[0]);
				listField.add(listCer[1]);
				listField.add(listCer[2]);
			}
		}
		int i = 1;
		for (String item : listItem) {
			if(i != listItem.size() && item != null && !item.isEmpty()) query+= item + " AND ";
			if(i == listItem.size() && item != null && !item.isEmpty()) query+= item;
			i++;
		}
		query+="";
		query+="";
		st = con.prepareStatement(query);

		i = 1;
		for (String key : listField) {
			st.setString(i, key);
			i++;
		}

		ResultSet rs = st.executeQuery();

		while (rs.next()) {
			User user = new User();
			user.setUserId(rs.getInt("id"));
//			user.setFullname(rs.getString("fullname"));
//			user.setGender(rs.getInt("sex"));
//			user.setBirth(rs.getString("birth"));
//			user.setAge(rs.getInt("age"));
//			user.setExperience(rs.getInt("experience"));
//			user.setDepartmentName(rs.getString("departmentName"));
//			user.setStatus(rs.getInt("status"));
//			user.setCreator(rs.getString("creator"));
//			user.setCreateDate(rs.getString("createdate"));
//			user.setEditor(rs.getString("editor"));
//			user.setEditDate(rs.getString("editdate"));
//			user.setDeleter(rs.getString("deleter"));
//			user.setDeleteDate(rs.getString("deleteDate"));
//			user.setUsername(rs.getString("username"));
//			user.setPassword(rs.getString("password"));
//			user.setEmployeeEditor(rs.getString("employeeEditor"));
//			user.setEmployeeEditDate(rs.getString("employeeEditDate"));
			list.add(user);
		}

		st.close();
		con.close();

		return list;
	}
}
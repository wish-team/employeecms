<%@page contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@include file="/header.jsp"%>
<!-- こっちの様式でいく -->
<!-- フォーム作成場合はこの行に注意 -->
<!-- <form method="POST" action="" class="form-actions"> -->

<div id="content">
	<h3>社員情報・職務経歴確認画面</h3>

	<c:choose>
		<c:when test="${userInfo != null}">
			<c:forEach var="item" items="${userInfo}">
				<form action="" method="get" id="employee-edit-form">
					<div class="table-responsive clearfix">
						<div class="fl-left employee-info">
							<table class="table list-table-wp">
								<tr>
									<td class="thead-text">氏名</td>
									<td class="tbody-text"><span>${item.getFullname()}</span></td>
								</tr>
								<tr>
									<td class="thead-text">部署</td>
									<td class="tbody-text"><span>${item.getDepartmentName()}</span></td>
								</tr>
								<tr>
									<td class="thead-text">生年月日</td>
									<td class="tbody-text"><span>${item.getBirth()}</span></td>
								</tr>
								<tr>
									<td>年齢</td>
									<td><span>${item.getAge()}</span></td>
									<td><label for="gender">性別:</label>
									<c:choose>
										<c:when test="${item.getGender()==0}">
											<td><span>男</span></td>
										</c:when>
										<c:when test="${item.getGender()==1}">
											<td><span>女</span></td>
										</c:when>
									</c:choose>
									<td><label for="">経験年数</label>
									<td><span>${item.getExperience()}</span></td>
								</tr>
							</table>
						</div>

						<div class="fl-left employee-certificate">
							<table class="table list-table-wp">
								<thead>
									<tr>
										<td class="tbody-body"><span>取得資格</span></td>
									</tr>
								</thead>
								<tbody>
									<c:forEach var="cer" items="${listCertificate}">
										<tr>
											<td class="tbody-text"><span>${cer.getCertificateName()}</span></td>
										</tr>
									</c:forEach>

								</tbody>
							</table>
						</div>

						<!-- ボタン押下時のリンク先を設定 -->
						<div class="fl-right list-operation">
							<!-- <a href="">戻る</a> -->
							<a href="${item.getWorkHistoryInsertUrl()}">追加</a>
							<a href="${item.getEditUrl()}">更新</a>
						</div>
					</div>

					<div class="table-responsive">
						<table class="table list-table-wp" id="employee-edit-table">
							<thead>
								<tr>
									<td><span class="thead-text">期間</span></td>
									<td><span class="thead-text">業種</span></td>
									<td><span class="thead-text">言語</span></td>
									<td><span class="thead-text">DB</span></td>
									<td><span class="thead-text">フレームワーク</span></td>
									<td><span class="thead-text">詳細</span></td>
								</tr>
							</thead>
							<c:choose>
								<c:when test="${listWorkHistory !=null }">
									<tbody>
										<c:forEach var="item" items="${listWorkHistory }">
											<tr>
												<td class="work-time">
													<div>
														<span class="tbody-text">${item.getStartTime()}</span>
													</div> <span>～</span>
													<div>
														<span class="tbody-text">${item.getEndTime()}</span>
													</div>
												</td>

												<td><span class="tbody-text">${item.getIndustryName()}</span></td>

												<td>
													<c:choose>
														<c:when test="${item.getListLanguage() != null}">
															<c:forEach var="language" items="${item.getListLanguage()}">
																<p class="tbody-text">${language.getLanguageName() }</p>
															</c:forEach>

														</c:when>
													</c:choose>
												</td>
												<td>
													<c:choose>
														<c:when test="${item.getListDatabase() != null}">
															<c:forEach var="database" items="${item.getListDatabase()}">
																<p class="tbody-text">${database.getDatabaseName() }</p>
															</c:forEach>
														</c:when>
													</c:choose>
												</td>
												<td>
													<c:choose>
														<c:when test="${item.getListFramework() != null}">
															<c:forEach var="framework" items="${item.getListFramework()}">
																<p class="tbody-text">${framework.getFrameworkName() }</p>
															</c:forEach>
														</c:when>
													</c:choose>
												</td>
													<td>
													<span class="tbody-text">${item.getDetail()}</span>
												</td>
											</tr>
										</c:forEach>
								</c:when>
							</c:choose>
							</tbody>
						</table>
					</div>
				</form>
			</c:forEach>
		</c:when>
	</c:choose>
</div>



<%@include file="/footer.jsp"%>
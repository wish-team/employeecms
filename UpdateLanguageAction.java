package admin.manager;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import bean.Error;
import dao.ManagerDAO;
import tool.Action;

public class UpdateLanguageAction extends Action {
//	@SuppressWarnings("unchecked")
	public String execute(HttpServletRequest request, HttpServletResponse response) throws Exception {

		Error er = new Error();

		int id = Integer.parseInt(request.getParameter("id"));
		String languageName = request.getParameter("languageName");
		languageName = languageName.trim();

		//空文字判定
		if(languageName.isEmpty()) {
			er.setError("言語名を入れてください");
			request.setAttribute("error", er);
			return "editLanguage.jsp";
		}

		ManagerDAO dao=new ManagerDAO();

		/*
		 *言語名の検索と比較
		 */

		if(dao.isExist("tbl_language", "languageName",languageName)){
				er.setError("すでに存在している言語名です。");
				request.setAttribute("error", er);
				return "editLanguage.jsp";
		}

		//daoのupdate処理を呼ぶ
		dao.dbUpdate("tbl_language","languagename",id, languageName);

		return "showMaster.jsp";
	}
}

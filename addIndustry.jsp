<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@include file="/header.jsp"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div id="content">

	<div class="add-industry">
		<form action="AddIndustry.action" method="post" id="add-industry-from">

	<div id="industry-input">
				<input type="text" name="id" placeholder="ID" id="industry-id">
				<input type="text" name="industryName" placeholder="業種名" id="industry-name">
	</div>

			<c:choose>
				<c:when test="${error != null}">
						${error.getError()}
					</c:when>
			</c:choose>

			<div class="insert-button">
				<input type="submit" value="追加" class="form-button">
				<a href="../manager/Master.action?table=3" class="form-button">キャンセル</a>
			</div>


		</form>
	</div>
</div>

<%@include file="/footer.jsp"%>
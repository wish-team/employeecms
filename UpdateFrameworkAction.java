package admin.manager;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import bean.Error;
import dao.ManagerDAO;
import tool.Action;

public class UpdateFrameworkAction extends Action {
//	@SuppressWarnings("unchecked")
	public String execute(HttpServletRequest request, HttpServletResponse response) throws Exception {

		Error er = new Error();

		String idStr = request.getParameter("id");
		String frameworkName = request.getParameter("frameworkName");
		frameworkName = frameworkName.trim();

		if (frameworkName.isEmpty()) {
			er.setError("フレームワーク名を入力してください");
			request.setAttribute("error", er);
			return "editFramework.jsp";
		}

		ManagerDAO dao=new ManagerDAO();

		/*
		 *ID・FW名の検索と比較
		 */
		if(dao.isExist("tbl_framework", "frameworkName",frameworkName)){
				er.setError("すでに存在しているFW名です。");
				request.setAttribute("error", er);
				return "editFramework.jsp";
		}

		//daoのupdae処理を呼ぶ
		dao.dbUpdate1("tbl_framework","frameworkname",idStr, frameworkName);

		return "showMaster.jsp";
	}
}

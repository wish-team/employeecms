package admin.manager;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import bean.Error;
import dao.ManagerDAO;
import tool.Action;
public class AddCertificateAction extends Action {
//	@SuppressWarnings("unchecked")
	public String execute(HttpServletRequest request, HttpServletResponse response) throws Exception {

		HashMap<String, String> map = new HashMap<String, String>();
		Error er = new Error();
		String certificateName = request.getParameter("certificateName");

		if(certificateName.isEmpty()) {
			er.setError("資格名を入れてください");
			request.setAttribute("error", er);
			return "addCertificate.jsp";
		}

		map.put("id", null);
		map.put("certificateName", certificateName);
		map.put("rank", null);
		ManagerDAO dao=new ManagerDAO();

		if(dao.isExist("tbl_certificate", "certificateName", certificateName)) {
			er.setError("存在");
			request.setAttribute("error", er);
			return "addCertificate.jsp";
		}
		dao.dbInsert("tbl_certificate", map, 1);
		return "showMaster.jsp";
	}


}

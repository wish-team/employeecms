<%@page contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@include file="/header.jsp"%>
<div id="content">
	<div class="table-title">
		<h3>元帳管理</h3>
	</div>

	<div class="master-name">
		<a href="../manager/Master.action?table=1">DB一覧</a>
		<a href="../manager/Master.action?table=2">言語一覧</a>
		<a href="../manager/Master.action?table=3">業務一覧</a>
		<a href="../manager/Master.action?table=4">FW一覧</a>
		<a href="../manager/Master.action?table=5">資格一覧</a>
		<a href="../manager/Master.action?table=6">部署一覧</a>
	</div>

<c:choose>
	<c:when  test="${database != null}">
		${database.getDatabaseTable()}
	</c:when>

	<c:when  test="${language != null}">
		${language.getLanguageTable()}
	</c:when>

	<c:when  test="${industry != null}">
		${industry.getIndustryTable()}
	</c:when>

	<c:when  test="${framework != null}">
		${framework.getFrameworkTable()}
	</c:when>

	<c:when  test="${certificate!= null}">
		${certificate.getCertificateTable()}
	</c:when>

	<c:when  test="${department != null}">
		${department.getDepartmentTable()}
	</c:when>

</c:choose>



</div>

<%@include file="/footer.jsp"%>

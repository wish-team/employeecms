package admin.manager;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import bean.Error;
import dao.ManagerDAO;
import tool.Action;

public class UpdateIndustryAction extends Action {
//	@SuppressWarnings("unchecked")
	public String execute(HttpServletRequest request, HttpServletResponse response) throws Exception {

		Error er = new Error();

		//int id= Integer.parseInt(request.getParameter("id"));
		String idStr = request.getParameter("id");
		String industryName = request.getParameter("industryName");

		industryName = industryName.trim();

		if (industryName.isEmpty()) {
			er.setError("業種名を入力してください");
			request.setAttribute("error", er);
			return "editIndustry.jsp";
		}

		ManagerDAO dao=new ManagerDAO();

		/*
		 *ID・業種名の検索と比較
		 */
		if(dao.isExist("tbl_industry", "industryName",industryName)){
				er.setError("すでに存在している業種名です。");
				request.setAttribute("error", er);
				return "editIndustry.jsp";
		}

		//daoのupdate処理を呼び出す
		dao.dbUpdate1("tbl_industry","industryname",idStr,industryName);

		return "showMaster.jsp";
	}
}

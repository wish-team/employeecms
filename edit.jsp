<%@page contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@include file="/header.jsp"%>
<!-- こっちの様式でいく -->
<!-- フォーム作成場合はこの行に注意 -->
<!-- <form method="POST" action="" class="form-actions"> -->

<div id="content">
	<h3>社員情報・職務経歴更新画面</h3>

	<c:choose>
		<c:when test="${userInfo != null}">
			<c:forEach var="item" items="${userInfo}">
				<form action="${item.getEditUserInfoUrl() }" method="get" id="employee-edit-form">
					<div class="table-responsive clearfix">
						<div class="fl-left employee-info">
							<table class="table list-table-wp">
								<tr>
									<td class="thead-text">氏名</td>
									<td class="tbody-text"><input type="text" name="fullname"
										value="${item.getFullname()}"></td>
								</tr>
								<tr>
									<td class="thead-text">部署</td>
									<td class="tbody-text"><input type="text"
										name="department" value="${item.getDepartmentName()}"></td>
								</tr>
								<tr>
									<td class="thead-text">生年月日</td>
									<td class="tbody-text"><input type="text" name="birth"
										value="${item.getBirth()}"></td>
								</tr>
								<tr>
									<td>年齢</td>
									<td><input type="text" name="birth"
										value="${item.getAge()}"></td>
									<td><label for="gender">性別:</label> <input type="radio"
										name="gender" value="0">男 <input type="radio"
										name="gender" value="1">女</td>

									<td><label for="">経験年数</label>
									<td><input type="text" name="birth"
										value="${item.getExperience()}"></td>
								</tr>
							</table>
						</div>

						<div class="fl-left employee-certificate">
							<table class="table list-table-wp">
								<thead>
									<tr>
										<td class="tbody-body"><span>取得資格</span></td>
									</tr>
								</thead>
								<tbody>
									<c:forEach var="cer" items="${listCertificate}">
										<tr>
											<td class="tbody-text"><select name="diplomaOp1">
													<option value="">${cer.getCertificateName()}</option>
													<option value="">Master から印刷</option>
											</select>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>

						<div class="fl-right list-operation">
							<input type="submit" name="btn-submit" value="社員情報更新">
						</div>
					</div>
				</form>

				<div>
					<c:choose>
						<c:when test="${listWorkHistory !=null }">
							<c:forEach var="item" items="${listWorkHistory }">
								<form action="${item.getEditUserWorkHistoryUrl() }" method="POST">
									<table class="table list-table-wp update-info">
										<thead>
											<tr>
												<td class="tbody-head"><span>期間</span></td>
												<td class="tbody-head"><span>業種</span></td>
												<td class="tbody-head"><span>言語</span></td>
												<td class="tbody-head"><span>Database</span></td>
												<td class="tbody-head"><span>フレームワーク</span></td>
												<td class="tbody-head"><span>詳細</span></td>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td class="work-time">
													<div>
														<input name="year" value="1999"><span>年</span> <input
															name="mounth" value="99"><span>月</span> <input
															name="day" value="99"><span>日</span>
													</div>

													<div>
														<input name="year" value="1999"><span>年</span> <input
															name="mounth" value="99"><span>月</span> <input
															name="day" value="99"><span>日</span>
													</div>
												</td>

												<td><select name="industry">
														<option value="id">--> ${item.getIndustryName()}
															<--</option>
														<c:forEach var="industryDb" items="${listIndustryDb}">
															<option value="${industryDb.getIdStr()}">${industryDb.getIndustryName()}</option>
														</c:forEach>

												</select></td>

												<td><c:choose>
														<c:when test="${item.getListLanguage() != null}">
															<c:forEach var="language"
																items="${item.getListLanguage()}">
																<select name="languageOp1">
																	<option value="languageId">->
																		${language.getLanguageName() } <-</option>
																	<c:forEach var="languageDb" items="${listLangugeDb}">
																		<option value="${languageDb.getId()}">${languageDb.getLanguageName()}</option>
																	</c:forEach>
																</select>
															</c:forEach>
														</c:when>
													</c:choose></td>
												<td><c:choose>
														<c:when test="${item.getListDatabase() != null}">
															<c:forEach var="database"
																items="${item.getListDatabase()}">
																<select name="databaseOp1">
																	<option value="databaseOp1">->
																		${database.getDatabaseName()} <-</option>
																	<c:forEach var="databaseDb" items="${listDatabaseDb}">
																		<option value="${databaseDb.getId()}">${databaseDb.getDatabaseName()}</option>
																	</c:forEach>
																</select>
															</c:forEach>
														</c:when>
													</c:choose></td>
												<td><c:choose>
														<c:when test="${item.getListFramework() != null}">
															<c:forEach var="framework"
																items="${item.getListFramework()}">
																<select name="databaseOp1">
																	<option value="databaseOp1">->
																		${framework.getFrameworkName()} <-</option>
																	<c:forEach var="frameworkDb" items="${listFrameworkDb}">
																		<option value="${frameworkDb.getIdStr()}">${frameworkDb.getFrameworkName()}</option>
																	</c:forEach>
																</select>
															</c:forEach>
														</c:when>
													</c:choose></td>
												<td><input name="detail" value="${item.getDetail()}">
												</td>
												<td>
													<input type="submit" name="btn-button" value="更新">
												</td>
											</tr>
										</tbody>
									</table>
								</form>
							</c:forEach>
						</c:when>
					</c:choose>
				</div>

			</c:forEach>
		</c:when>
	</c:choose>
</div>



<%@include file="/footer.jsp"%>
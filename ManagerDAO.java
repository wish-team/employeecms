package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import bean.Manager;

public class ManagerDAO extends DAO {
	public List<Manager> dbGetData(String querry, String where, String option) throws Exception {
		List<Manager> list = new ArrayList<>();

		Connection con = getConnection();
		PreparedStatement st = null;

		if ((where == null) || (where.isEmpty()))
			st = con.prepareStatement(querry);
		if ((where != null)) {
			st = con.prepareStatement(querry + " where ?");
			st.setString(1, where);
		}

		ResultSet rs = st.executeQuery();

		if (option.equals("database")) {
			while (rs.next()) {
				Manager managerData = new Manager();
				managerData.setId(rs.getInt("id"));
				managerData.setDatabaseName(rs.getString("databaseName"));
				list.add(managerData);
			}
		}

		if (option.equals("language")) {
			while (rs.next()) {
				Manager managerData = new Manager();
				managerData.setId(rs.getInt("id"));
				managerData.setLanguageName(rs.getString("languageName"));
				list.add(managerData);
			}
		}

		if (option.equals("framework")) {
			while (rs.next()) {
				Manager managerData = new Manager();
				managerData.setIdStr(rs.getString("id"));
				managerData.setFrameworkName(rs.getString("frameworkName"));
				list.add(managerData);
			}
		}

		if (option.equals("department")) {
			while (rs.next()) {
				Manager managerData = new Manager();
				managerData.setId(rs.getInt("id"));
				managerData.setDepartmentName(rs.getString("departmentName"));
				list.add(managerData);
			}
		}

		if (option.equals("certificate")) {
			while (rs.next()) {
				Manager managerData = new Manager();
				managerData.setId(rs.getInt("id"));
				managerData.setCertificateName(rs.getString("certificateName"));
				list.add(managerData);
			}
		}

		if (option.equals("industry")) {
			while (rs.next()) {
				Manager managerData = new Manager();
				managerData.setIdStr(rs.getString("id"));
				managerData.setIndustryName(rs.getString("industryName"));
				list.add(managerData);
			}
		}

		st.close();
		con.close();

		return list;
	}

	public int dbInsert(String table, HashMap<String, String> map, int option) throws Exception {

		String query = "insert into " + table + " (";
		int trigger = 1;
		for (String key : map.keySet()) {
			if (trigger == map.size()) {
				query += key + ") ";
			} else {
				query += key + ",";
			}
			trigger++;
		}
		trigger = 1;
		query += "values (";
		if (option == 1) {
			for (String key : map.keySet()) {
				String value = map.get(key);
				if (trigger == map.size()) {
					if (value == null) {
						query += "null)";
					} else {
						query += "?)";
					}
				} else {
					if (value == null) {
						query += "null,";
					} else {
						query += "? ,";
					}
				}
				trigger++;
			}
		}

		if (option == 2) {
			for (String key : map.keySet()) {
				System.out.println(key);
				if (trigger == map.size()) {
					query += "? )";
				} else {
					query += "?, ";
				}
				trigger++;
			}
		}

		Connection con = getConnection();
		con.setAutoCommit(false);
		PreparedStatement st = con.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
		int i = 1;
		for (String key : map.keySet()) {
			String check = map.get(key);

			if ((check != null)) {
				st.setString(i, map.get(key));
				i++;
			}
		}
		int line = st.executeUpdate();
		ResultSet rs = st.getGeneratedKeys();
		rs.next();
		int primaryKey = -1;
		if (option == 1)
			primaryKey = rs.getInt(1);

		st.close();

		if (line != 1) {
			con.rollback();
			con.setAutoCommit(true);
			con.close();
		}
		con.commit();
		con.setAutoCommit(true);
		con.close();
		return primaryKey;
	}

	public boolean isExist(String table, String key, String value) throws Exception {
		Connection con = getConnection();
		PreparedStatement st = con.prepareStatement("select * from " + table + " where " + key + " =?");
		st.setString(1, value);
		ResultSet rs = st.executeQuery();

		if (rs.next()) {
			st.close();
			con.close();
			return true;
		}

		st.close();
		con.close();
		return false;

	}

}

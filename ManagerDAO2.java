package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import bean.User;

public class ManagerDAO2 extends DAO{
	public List<User> dbGetData(int userId) throws Exception {
		List<User> list=new ArrayList<>();

		Connection con=getConnection();
		PreparedStatement st = con.prepareStatement("select id, certificateId from tbl_user as us join tbl_certificate_detail as cd on ? = ? where us.id=?");
		st.setInt(1, userId);
		st.setInt(2, userId);
		st.setInt(3, userId);
		ResultSet rs = st.executeQuery();

		while (rs.next()) {
			User u = new User();
			u.setId(rs.getInt("Id"));
			u.setCertificateId(rs.getInt("CertificateId"));
			list.add(u);
		}

		st.close();
		con.close();

		return list;
	}

	public List<User> dbUpdate(int certificate_id, int userId, int id) throws Exception {
		List<User> list=new ArrayList<>();

		Connection con=getConnection();
		PreparedStatement st = con.prepareStatement("UPDATE tbl_certificate_detail SET certificate_id=? WHERE user_id = ? and id=?");
		st.setInt(1, certificate_id);
		st.setInt(2, userId);
		st.setInt(3, id);


		st.close();
		con.close();

		return list;
	}

}

	
	
	UseDAO内のInsertメソッドを以下に上書きしてください
	
	
	/**
	 * Userテーブル、資格テーブルに社員情報を登録する処理
	 * @param user
	 * @return line,line2(段階毎に登録成功したレコード数、失敗なら0が返る
	 * @throws Exception
	 */
	public int insert(User user) throws Exception {


		Connection con = getConnection();

		//オートコミットをオフ
		con.setAutoCommit(false);

		//SQL処理1回目、セッションのLoginIDよりログイン中の管理者のIDを判別
		//SQL文を保持するオブジェクトを生成し、値を代入
		PreparedStatement creator = con.prepareStatement(
				"SELECT ID FROM TBL_ADMIN WHERE USERNAME=?");
		creator.setString(1, user.getCreator());
		ResultSet c_id = creator.executeQuery();

		//ログイン中の管理者のIDを代入
		Manager manager = null;

		while (c_id.next()) {
			manager = new Manager();
			manager.setId(c_id.getInt("id"));

		}

		int creatorid = manager.getId();

		//SQL処理2回目、USERテーブルに値を登録する
		PreparedStatement st = con.prepareStatement(
				"INSERT INTO TBL_USER "
						+ "(ID,FULLNAME,SEX,BIRTH,AGE,EXPERIENCE,DEPARTMENT,STATUS,CREATOR,CREATEDATE,USERNAME,PASSWORD)"
						+ "VALUES (null,?,?,?,?,?,?,?,?,CURRENT_TIMESTAMP,?,?)");
		st.setString(1, user.getFullname());
		st.setInt(2, user.getGender());
		st.setDate(3, (Date) user.getBirth_date());
		st.setInt(4, user.getAge());
		st.setInt(5, user.getExperience());
		st.setInt(6, user.getDepartmentId());	 //参照整合があるので実在しないものを指定するとエラー
		st.setInt(7, 0); 						//削除フラグなので登録時は必ず0
		st.setInt(8, creatorid); 				//参照整合があるので実在しないものを指定するとエラー
		st.setString(9, user.getUsername());
		st.setString(10, user.getPassword());
		int line = st.executeUpdate();

		//登録完了したかの分岐
		if (line > 0) {

			//SQL処理3回目、資格テーブルへ値を登録する為の新規の登録した技術者のIDを判別 //資格テーブル

			//SQL文を保持するオブジェクトを生成し、値を代入
			PreparedStatement u_id;
			u_id = con.prepareStatement(
					"SELECT ID FROM TBL_USER WHERE USERNAME=? AND PASSWORD=?");
			u_id.setString(1, user.getUsername());
			u_id.setString(2, user.getPassword());

			//実行結果を保持するオブジェクトを生成し、値を代入
			ResultSet newid = u_id.executeQuery();


			//実行結果からそれぞれidを取得し、宣言していたusrid変数に入れる

			while (newid.next()) {
				manager = new Manager();
				manager.setId(newid.getInt("id"));
			}

			int Usrid = manager.getId();
			int[] certificate_ids = user.getCertificate_ids();

			//SQL処理4回目、資格テーブルに資格の数の回数登録処理を行う。
			int line2 = 1;
			for (int i = 0; i < certificate_ids.length; i++) {
				PreparedStatement st2 = con.prepareStatement(
						"INSERT INTO TBL_CERTIFICATE_DETAIL (USERID,CERTIFICATEID) VALUES (?,?)");
				st2.setInt(1, Usrid);
				st2.setInt(2, certificate_ids[i]);
				if(certificate_ids[i] != 0) {
					line2 = st2.executeUpdate();
				}

				//条件分岐
				if (line2 == 0) {
					break;
				}
			}

			//すべて更新完了したらオートコミットをオンにしてDB接続を切る
			if (line2 > 0) {
				con.commit();
				con.setAutoCommit(true);
				st.close();
				con.close();

			}else {
				con.rollback();
				con.setAutoCommit(true);
				st.close();
				con.close();
			}

			return line2; //登録失敗の場合line=0 , 成功の場合登録した数を返す

		} else {
			con.rollback();
			con.setAutoCommit(true);
			st.close();
			con.close();

			return line; //登録失敗の場合line=0を返す
		}


	}
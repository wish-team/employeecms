<%@page contentType="text/html; charset=UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Wish Team Project</title>

<style>
	<%@include file="/css/reset.css"%>
	<%@include file="/css/global.css"%>
	<%@include file="/css/style.css"%>
</style>

</head>

<body>
	<div id="wrapper">
		<div class="wp-inner">
			<div id="content">
				<div class="login-title clearfix">
					<p class="fl-left">技術者用</p>
				</div>

				<div class="table-title">
					<p>ログイン画面</p>
				</div>

				<div class="login-notice">
					<p>IDとPasswordを入力してください。</p>
				</div>

				<div class="login-form">
					<form action="../user/Login.action" method="post">
						<p class="f-login">
							ログインID：<input type="text" name="username">
						<p>
						<p class="f-login">
							パスワード：<input type="password" name="password">
						<p>
						<p class="f-login">
							<input type="submit" value="ログイン">
						<p>
					</form>
				</div>
			</div>
		</div>
	</div>
</body>

</html>












package user;

import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import bean.User;
import dao.UserDAO;
import tool.Action;

public class DetailAction extends Action{
	public String execute(
			HttpServletRequest request, HttpServletResponse response
	) throws Exception {

		//HttpSession session=request.getSession();

		String username =request.getParameter("username");
		UserDAO dao=new UserDAO();
		HashMap<String, String> whereInfo = new HashMap<String, String>();
		whereInfo.put("username",username);

		List<User> userInfo = dao.dbGetData("select * from tbl_user as us JOIN tbl_department as dp ON us.department = dp.id ", whereInfo, "user");
		String id = Integer.toString((userInfo.get(0)).getUserId());
		HashMap<String, String> where = new HashMap<String, String>();
		where.put("userId", id);
		List<User> listCertificate = dao.dbGetData("select * from tbl_certificate_detail as dt INNER JOIN tbl_certificate as c ON dt.certificateId = c.id", where, "listCertificate");
		List<User> listWorkHistory = dao.dbGetData("select * from tbl_work_history as wh join tbl_industry as i on wh.industryId = i.id", where, "workHistory");

		for(User item : listWorkHistory) {
			String workHistoryId = Integer.toString(item.getWorkHistoryId());
			HashMap<String, String> whereWorkHistory = new HashMap<String, String>();
			whereWorkHistory.put("workHistoryId", workHistoryId);
			List<User> listLanguage = dao.dbGetData("select languageName, la.id from tbl_work_history_detail as hd join tbl_language as la on hd.languageId = la.id", whereWorkHistory, "listLanguage");
			List<User> listDatabase = dao.dbGetData("select databaseName, db.id from tbl_work_history_detail as hd join tbl_database as db on hd.databaseId = db.id", whereWorkHistory, "listDatabase");
			List<User> listFramework = dao.dbGetData("select frameworkName, fw.id from tbl_work_history_detail as hd join tbl_framework as fw on hd.frameworkId = fw.id", whereWorkHistory, "listFramework");
			item.setListLanguage(listLanguage);
			item.setListDatabase(listDatabase);
			item.setListFramework(listFramework);
		}

		for(User item : userInfo) {
			item.setWorkHistoryInsertUrl("../user/WorkHistoryInsert.action?id=" + id);
			item.setEditInfoUrl("../user/Edit.action?id=" + id);
		}

		request.setAttribute("userInfo", userInfo);
		request.setAttribute("listCertificate", listCertificate);
		request.setAttribute("listWorkHistory", listWorkHistory);

		return "../user/detail.jsp";
	}
}

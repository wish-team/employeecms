package user;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import bean.Manager;
import bean.User;
import dao.ManagerDAO;
import dao.UserDAO;
import tool.Action;

public class WorkHistoryInsertAction extends Action {

	public String execute(
			HttpServletRequest request, HttpServletResponse response) throws Exception {

		ManagerDAO dao = new ManagerDAO();
		UserDAO Udao = new UserDAO();
		String userId = request.getParameter("id");
		String btn = request.getParameter("btn-submit");


		if (btn == null) btn = "";
		if ((!btn.isEmpty()) || (btn.equals("submit"))) {


			String startYear = request.getParameter("StartYear");
			String startMonth = request.getParameter("StartMonth");
			String startDay = request.getParameter("StartDay");
			String startTime = startYear + "-" + startMonth + "-" + startDay;
			//int StartTime = Integer.parseInt(JoinStartTime);
			//期間の終了日を取得してyyyy/mm/ddに変換
			String endYear = request.getParameter("EndYear");
			String endMonth = request.getParameter("EndMonth");
			String endDay = request.getParameter("EndDay");
			String endTime = endYear + "-" + endMonth + "-" + endDay;

			//int EndTime = Integer.parseInt(JoinEndTime);
			//職務経歴の項目を取得
			String industry = request.getParameter("industry");
			String detail = request.getParameter("detail");
			String[] language = request.getParameterValues("language");
			String[] database = request.getParameterValues("database");
			String[] framework = request.getParameterValues("framework");

			//期間に入力漏れがあった場合、エラーを表示
			if (startYear.isEmpty() || startMonth.isEmpty() || startDay.isEmpty() || endYear.isEmpty() || endMonth.isEmpty() || endDay.isEmpty()) {
				return "";
			}
			HashMap<String, String> map = new HashMap<String, String>();

			//期間、業種、ID、詳細を追加
			map.put("workhistoryid", null);
			map.put("industryid", industry);
			map.put("userid", userId);
			map.put("detail", detail);
			map.put("starttime", startTime);
			map.put("endtime", endTime);

			int workHistoryId = Udao.dbInsert("tbl_work_history", map, 1);

			//言語、DB、フレームワークを追加
			HashMap<String, String> mapDetail = new HashMap<String, String>();
			mapDetail.put("id", null);
			mapDetail.put("workHistoryId", Integer.toString(workHistoryId));
			for (int i = 0; i < 3; i++) {
				if (!language[i].isEmpty()) {
					mapDetail.put("languageId", language[i]);
				} else {
					mapDetail.put("languageId", null);
				}
				if (!database[i].isEmpty()) {
					mapDetail.put("databaseId", database[i]);
				} else {
					mapDetail.put("databaseId", null);
				}
				if (!framework[i].isEmpty()) {
					mapDetail.put("frameworkId", framework[i]);
				} else {
					mapDetail.put("frameworkId", null);
				}
				if (language[i].isEmpty() && database[i].isEmpty() && framework[i].isEmpty()) {

				} else
					Udao.dbInsert("tbl_work_history_detail", mapDetail, 1);
			}
			HashMap<String, String> whereUser = new HashMap<String, String>();
			whereUser.put("us.id", userId);
			List<User> userInfo = Udao.dbGetData("select * from tbl_user as us JOIN tbl_department as dp ON us.department = dp.id ", whereUser, "user");
			String username = (userInfo.get(0)).getUsername();
			return "../user/Detail.action?username=" + username;

		}

		HashMap<String, String> whereUserInfo = new HashMap<String, String>();
		List<User> userInfo = new ArrayList<User>();
		List<Manager> listLanguage = new ArrayList<Manager>();
		List<Manager> listDatabase = new ArrayList<Manager>();
		List<Manager> listFramework = new ArrayList<Manager>();
		List<Manager> listIndustry = new ArrayList<Manager>();

		whereUserInfo.put("us.id", userId);
		userInfo = Udao.dbGetData("select * from TBL_USER as us JOIN TBL_DEPARTMENT as dp ON us.department= dp.id ",
				whereUserInfo, "user");
		request.setAttribute("userInfo", userInfo);
		User user = userInfo.get(0);
		user.setWorkHistoryInsertUrl("../user/WorkHistoryInsert.action?id=" + userId);
		request.setAttribute("user", user);
		String birth = user.getBirth();
		//(1)分割する文字列を生成
		String str = new String(birth);
		//(2)文字列をsplitメソッドで分割
		String[] strAry = str.split("-");

		request.setAttribute("year", strAry[0]);
		request.setAttribute("month", strAry[1]);
		request.setAttribute("day", strAry[2]);

		int gender = user.getGender();

		if (gender == 0) {
			request.setAttribute("Gender", "男");
		} else {
			request.setAttribute("Gender", "女");
		}

		HashMap<String, String> where = new HashMap<String, String>();

		where.put("userid", userId);
		//workHistoryInfo = Udao.dbGetData("select * from tbl_work_history ", where, "workhistory");
		//request.setAttribute("workHistoryInfo", workHistoryInfo);

		List<User> listCertificate = Udao.dbGetData(
				"select * from tbl_certificate_detail as dt INNER JOIN tbl_certificate as c ON dt.certificateId = c.id",
				where, "listCertificate");

		int i = 1;
		for (User item : listCertificate) {
			request.setAttribute("certificate" + i, item.getCertificateName());
			i++;
		}

		request.setAttribute("listCertificate", listCertificate);

		listLanguage = dao.dbGetData("select * from tbl_language", null, "language");
		request.setAttribute("listLanguage", listLanguage);

		listDatabase = dao.dbGetData("select * from tbl_database", null, "database");
		request.setAttribute("listDatabase", listDatabase);

		listFramework = dao.dbGetData("select * from tbl_framework", null, "framework");
		request.setAttribute("listFramework", listFramework);

		listIndustry = dao.dbGetData("select * from tbl_industry", null, "industry");
		request.setAttribute("listIndustry", listIndustry);

		request.setAttribute("listIndustry", listIndustry);


		return "../user/workHistoryInsert.jsp";

	}
}

package user;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import tool.Action;

/**
 * ログアウト実行
 * @return ユーザログアウト結果をlogout-out.jspへ返す
 * @return 失敗の場合：結果をlogout-error.jspへ返す
 */
public class LogoutAction extends Action {
	public String execute(
		HttpServletRequest request, HttpServletResponse response
	) throws Exception {

		HttpSession session=request.getSession();

		if ( session.getAttribute("username") != null) {
			session.removeAttribute("username");
			session.removeAttribute("isLogin");
		}
		return "../user/login.jsp";
	}
}

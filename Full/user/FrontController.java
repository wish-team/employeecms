package tool;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(urlPatterns={"*.action"})
public class FrontController extends HttpServlet {

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out=response.getWriter();
		try {
			HttpSession session = request.getSession();

			String test = (String) session.getAttribute("username");
			String path=request.getServletPath().substring(1);
			String name=path.replace(".a", "A").replace('/', '.');
			Action action=(Action)Class.forName(name).newInstance();
			String url=action.execute(request, response);
			if(test == null) test ="";

			if(path.equals("user/Login.action")) {
				if((test.isEmpty()) && (!path.equals("user/Login.action"))) {
					request.getRequestDispatcher("../user/login.jsp").forward(request, response);
				}
				request.getRequestDispatcher(url).forward(request, response);
			}

			if ((test.isEmpty()) && (!path.equals("admin/manager/Login.action"))) {
				request.getRequestDispatcher("../manager/login.jsp").forward(request, response);
			}else {
				request.getRequestDispatcher(url).forward(request, response);
			}

		} catch (Exception e) {
			e.printStackTrace(out);
		}
	}

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}
}

<%@page contentType="text/html; charset=UTF-8"%>
<%@ include file="/headerUser.jsp"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div class="table-title">
	<h3>職務経歴追加画面</h3>
</div>
<div class="insert-form-content">
	<div class="insert-form-wp">
		<div class="employee-info">

			<table border=1 class="employee-info-table">


			<%-- <c:if>--%>
				<c:forEach var="item" items="${userInfo}">

					<thead>
					<tr>
						<!-- 職務経歴更新画面は注意 -->
						<!-- <td><input type="checkbox" name="checkAll" id="checkAll"></td> -->

						<td><span class="thead-text">氏名</span></td>
						<td colspan="5">${item.getFullname()}</td>
						<td><span class="thead-text">取得資格</span></td>
					</tr>
					<tr>
						<td><span class="thead-text">部署</span></td>
						<td></td><td></td><td></td><td></td>
						<td>${item.getDepartmentName()}</td>
						<td colspan="5">${certificate1}</td>

					</tr>
					<tr>
						<td><span class="thead-text">生年月日</span></td>
						<td colspan="2">${year}年</td>
						<td colspan="2">${month}月</td>
						<td>${day}日</td>
						<td><span class="thead-text">${certificate2}</span></td>
					</tr>
					<tr>
						<td width="10%"><span class="thead-text">年齢</span></td>
						<td width="10%">${item.getAge()}</td>
						<td width="10%"><span class="thead-text">性別</span></td>
						<td width="10%">${Gender}</td>
						<td width="10%"><span class="thead-text">経験年数</span></td>
						<td width="10%">${item.getExperience()}</td>
						<td width="40%">${certificate3}</td>
					</tr>
					</c:forEach>
				</thead>
			<%-- </c:if>--%>


			</table>
		</div>
		<div class="search-form-content">
			<div class="search-form">
			<c:set var ="url" value="${user.getWorkHistoryInsertUrl()}"></c:set>
				<form method="POST" action="${url}" class="form-actions">
					<table border=1 class="table">
						<thead>
							<tr>
								<td><span class="thead-text2">期間</span></td>
								<td><span class="thead-text2">業種</span></td>
								<td><span class="thead-text2">言語</span></td>
								<td><span class="thead-text2">DB</span></td>
								<td><span class="thead-text2">フレームワーク</span></td>
								<td><span class="thead-text2t">詳細</span></td>
							</tr>
							<tr>
								<td><div class=textfix><select name="StartYear">
										<option value="">--</option>
										<%
										for (int i = 1900; i <= 2020; i++) {
										%>
										<option value="<%=i%>"><%=i%></option>
										<%
										}
										%>
								</select><span>年</span> <select name="StartMonth">
										<option value="">--</option>
										<%
										for (int i = 1; i <= 12; i++) {
										%>
										<option value="<%=i%>"><%=i%></option>
										<%
										}
										%>
								</SELECT><span>月</span> <select name="StartDay">
										<option value="">--</option>
										<%
										for (int i = 1; i <= 31; i++) {
										%>
										<option value="<%=i%>"><%=i%></option>
										<%
										}
										%>
								</select><span>日</span><br> <span>～</span>
								 <br> <select name="EndYear">
										<option value="">--</option>
										<%
										for (int i = 1900; i <= 2020; i++) {
										%>
										<option value="<%=i%>"><%=i%></option>
										<%
										}
										%>
								</select><span>年</span> <select name="EndMonth">
										<option value="">--</option>
										<%
										for (int i = 1; i <= 12; i++) {
										%>
										<option value="<%=i%>"><%=i%></option>
										<%
										}
										%>
								</SELECT><span>月</span> <select name="EndDay">
										<option value="">--</option>
										<%
										for (int i = 1; i <= 31; i++) {
										%>
										<option value="<%=i%>"><%=i%></option>
										<%
										}
										%>
								</select><span>日</span><br></div></td>
								<td><select name="industry">
										<option value="">--</option>
										<c:forEach var="item" items="${listIndustry}">
											<option value="${item.getIdStr()}">${item.getIndustryName()}</option>
										</c:forEach>
								</select></td>
								<td><select name="language">
								<option value="">--</option>
										<c:forEach var="item" items="${listLanguage}">
											<option value="${item.getId()}">${ item.getLanguageName()}</option>
										</c:forEach>
								</select> <br> <select name="language">
										<option value="">--</option>
										<c:forEach var="item" items="${listLanguage}">
											<option value="${item.getId()}">${ item.getLanguageName()}</option>
										</c:forEach>
								</select> <br> <select name="language">
										<option value="">--</option>
										<c:forEach var="item" items="${listLanguage}">
											<option value="${item.getId()}">${ item.getLanguageName()}</option>
										</c:forEach>
								</select></td>
								<td><select name="database">
										<option value="">--</option>
										<c:forEach var="item" items="${listDatabase}">
											<option value="${item.getId()}">${ item.getDatabaseName()}</option>
										</c:forEach>
								</select> <br> <select name="database">
										<option value="">--</option>
										<c:forEach var="item" items="${listDatabase}">
											<option value="${item.getId()}">${ item.getDatabaseName()}</option>
										</c:forEach>
								</select> <br> <select name="database">
										<option value="">--</option>
										<c:forEach var="item" items="${listDatabase}">
											<option value="${item.getId()}">${ item.getDatabaseName()}</option>
										</c:forEach>
								</select></td>
								<td><select name="framework">
										<option value="">--</option>
										<c:forEach var="item" items="${listFramework}">
											<option value="${item.getIdStr()}">${ item.getFrameworkName()}</option>
										</c:forEach>
								</select> <br> <select name="framework">
										<option value="">--</option>
										<c:forEach var="item" items="${listFramework}">
											<option value="${item.getIdStr()}">${ item.getFrameworkName()}</option>
										</c:forEach>
								</select> <br> <select name="framework">
										<option value="">--</option>
										<c:forEach var="item" items="${listFramework}">
											<option value="${item.getIdStr()}">${ item.getFrameworkName()}</option>
										</c:forEach>
								</select></td>
								<td><textarea name="detail" cols="30" rows="5"></textarea>
								</td>


							</tr>
						</thead>
					</table>
					<div class="search-form2">
						<button type="submit" name="btn-submit" value="submit">登録</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

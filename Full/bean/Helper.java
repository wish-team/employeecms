package bean;

public class Helper implements java.io.Serializable {



	public boolean isNormalText(String str) {
		char[] chars = str.toCharArray();
		for (int i = 0; i < chars.length; i++) {
			char c = chars[i];
			if ((c <= '\u007e') || (c == '\u00a5') || (c == '\u203e') || (c >= '\uff61' && c <= '\uff9f')) return true;
		}
		return false;
	}
}


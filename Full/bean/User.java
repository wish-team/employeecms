package bean;

import java.util.Date;
import java.util.List;

public class User implements java.io.Serializable {

	private int languageId;
	private int databaseId;
	private int certificateId;
	private int frameworkId;
	private int industryId;
	private int workHistoryId;
	private int departmentId;
	private int userId;
	private List<User> listCertificate;
	private List<User> listLanguage;
	private List<User> listDatabase;
	private List<User> listFramework;
	private String workHistoryInsertUrl;
	private String frameworkName;
	private int id;
	private String idStr;
	private Date birth_date;
	private int[] certificate_ids;
	private String editUserWorkHistoryUrl;
	private String editUserInfoUrl;
	private String yearStart;
	private String mounthStart;
	private String dayStart;
	private String yearEnd;
	private String mounthEnd;
	private String dayEnd;
	private String deleteInfoUrl;

	public String getDeleteInfoUrl() {
		return deleteInfoUrl;
	}
	public void setDeleteInfoUrl(String deleteInfoUrl) {
		this.deleteInfoUrl = deleteInfoUrl;
	}
	public String getYearStart() {
		return yearStart;
	}
	public void setYearStart(String yearStart) {
		this.yearStart = yearStart;
	}
	public String getMounthStart() {
		return mounthStart;
	}
	public void setMounthStart(String mounthStart) {
		this.mounthStart = mounthStart;
	}
	public String getDayStart() {
		return dayStart;
	}
	public void setDayStart(String dayStart) {
		this.dayStart = dayStart;
	}
	public String getYearEnd() {
		return yearEnd;
	}
	public void setYearEnd(String yearEnd) {
		this.yearEnd = yearEnd;
	}
	public String getMounthEnd() {
		return mounthEnd;
	}
	public void setMounthEnd(String mounthEnd) {
		this.mounthEnd = mounthEnd;
	}
	public String getDayEnd() {
		return dayEnd;
	}
	public void setDayEnd(String dayEnd) {
		this.dayEnd = dayEnd;
	}
	public String getIdStr() {
		return idStr;
	}
	public void setIdStr(String idStr) {
		this.idStr = idStr;
	}
	public String getEditUserWorkHistoryUrl() {
		return editUserWorkHistoryUrl;
	}
	public void setEditUserWorkHistoryUrl(String editUserWorkHistoryUrl) {
		this.editUserWorkHistoryUrl = editUserWorkHistoryUrl;
	}
	public String getEditUserInfoUrl() {
		return editUserInfoUrl;
	}
	public void setEditUserInfoUrl(String editUserInfoUrl) {
		this.editUserInfoUrl = editUserInfoUrl;
	}
	public int[] getCertificate_ids() {
		return certificate_ids;
	}
	public void setCertificate_ids(int[] certificate_ids) {
		this.certificate_ids = certificate_ids;
	}

	public Date getBirth_date() {
		return birth_date;
	}
	public void setBirth_date(Date birth_date) {
		this.birth_date = birth_date;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getFrameworkName() {
		return frameworkName;
	}
	public void setFrameworkName(String frameworkName) {
		this.frameworkName = frameworkName;
	}
	public String getWorkHistoryInsertUrl() {
		return workHistoryInsertUrl;
	}
	public void setWorkHistoryInsertUrl(String workHistoryInsertUrl) {
		this.workHistoryInsertUrl = workHistoryInsertUrl;
	}
	public String getEditInfoUrl() {
		return editInfoUrl;
	}
	public void setEditInfoUrl(String editInfoUrl) {
		this.editInfoUrl = editInfoUrl;
	}
	private String editInfoUrl;



	public List<User> getListLanguage() {
		return listLanguage;
	}
	public void setListLanguage(List<User> listLanguage) {
		this.listLanguage = listLanguage;
	}
	public List<User> getListDatabase() {
		return listDatabase;
	}
	public void setListDatabase(List<User> listDatabase) {
		this.listDatabase = listDatabase;
	}
	public List<User> getListFramework() {
		return listFramework;
	}
	public void setListFramework(List<User> listFramework) {
		this.listFramework = listFramework;
	}
	private int numRow;

	public int getNumRow() {
		return numRow;
	}
	public void setNumRow(int numRow) {
		this.numRow = numRow;
	}
	public List<User> getListCertificate() {
		return listCertificate;
	}
	public void setListCertificate(List<User> list) {
		this.listCertificate = list;
	}
	public int getCertificateId() {
		return certificateId;
	}
	public void setCertificateId(int certificateId) {
		this.certificateId = certificateId;
	}
	public int getLanguageId() {
		return languageId;
	}
	public void setLanguageId(int languageId) {
		this.languageId = languageId;
	}
	public int getDatabaseId() {
		return databaseId;
	}
	public void setDatabaseId(int databaseId) {
		this.databaseId = databaseId;
	}
	public int getFrameworkId() {
		return frameworkId;
	}
	public void setFrameworkId(int frameworkId) {
		this.frameworkId = frameworkId;
	}
	public int getIndustryId() {
		return industryId;
	}
	public void setIndustryId(int industryId) {
		this.industryId = industryId;
	}
	public int getWorkHistoryId() {
		return workHistoryId;
	}
	public void setWorkHistoryId(int workHistoryId) {
		this.workHistoryId = workHistoryId;
	}
	public int getDepartmentId() {
		return departmentId;
	}
	public void setDepartmentId(int departmentId) {
		this.departmentId = departmentId;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public String getFullname() {
		return fullname;
	}
	public void setFullname(String fullname) {
		this.fullname = fullname;
	}
	public int getGender() {
		return gender;
	}
	public void setGender(int gender) {
		this.gender = gender;
	}
	public String getBirth() {
		return birth;
	}
	public void setBirth(String birth) {
		this.birth = birth;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int i) {
		this.age = i;
	}
	public int getExperience() {
		return experience;
	}
	public void setExperience(int experience) {
		this.experience = experience;
	}
	public String getDepartmentName() {
		return departmentName;
	}
	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public String getCreator() {
		return creator;
	}
	public void setCreator(String i) {
		this.creator = i;
	}
	public String getCreateDate() {
		return createDate;
	}
	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}
	public String getEditor() {
		return editor;
	}
	public void setEditor(String editor) {
		this.editor = editor;
	}
	public String getEditDate() {
		return editDate;
	}
	public void setEditDate(String editDate) {
		this.editDate = editDate;
	}
	public String getDeleter() {
		return deleter;
	}
	public void setDeleter(String deleter) {
		this.deleter = deleter;
	}
	public String getDeleteDate() {
		return deleteDate;
	}
	public void setDeleteDate(String deleteDate) {
		this.deleteDate = deleteDate;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getCertificateName() {
		return certificateName;
	}
	public void setCertificateName(String certificateName) {
		this.certificateName = certificateName;
	}
	public String getIndustryName() {
		return industryName;
	}
	public void setIndustryName(String industryName) {
		this.industryName = industryName;
	}
	public String getDetail() {
		return detail;
	}
	public void setDetail(String detail) {
		this.detail = detail;
	}
	public String getStartTime() {
		return startTime;
	}
	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}
	public String getEndTime() {
		return endTime;
	}
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}
	public String getLanguageName() {
		return languageName;
	}
	public void setLanguageName(String languageName) {
		this.languageName = languageName;
	}
	public String getDatabaseName() {
		return databaseName;
	}
	public void setDatabaseName(String databaseName) {
		this.databaseName = databaseName;
	}
	public String getFramworkName() {
		return framworkName;
	}
	public void setFramworkName(String framworkName) {
		this.framworkName = framworkName;
	}
	private String fullname;
	private int gender;
	private String birth;
	private int age;
	private int experience;
	private String departmentName;
	private int status;
	private String creator;
	private String createDate;
	private String editor;
	private String editDate;
	private String deleter;
	private String deleteDate;
	private String username;
	private String password;
	private String certificateName;
	private String industryName;
	private String detail;
	private String startTime;
	private String endTime;
	private String languageName;
	private String databaseName;
	private String framworkName;
	private String EmployeeEditor;
	private String EmployeeEditDate;
	private String detailUrl;
	private String editUrl;
	private String deleteUrl;


	public String getDetailUrl() {
		return detailUrl;
	}
	public void setDetailUrl(String detailUrl) {
		this.detailUrl = detailUrl;
	}
	public String getEditUrl() {
		return editUrl;
	}
	public void setEditUrl(String editUrl) {
		this.editUrl = editUrl;
	}
	public String getDeleteUrl() {
		return deleteUrl;
	}
	public void setDeleteUrl(String deleteUrl) {
		this.deleteUrl = deleteUrl;
	}
	public String getEmployeeEditor() {
		return EmployeeEditor;
	}
	public void setEmployeeEditor(String employeeEditor) {
		EmployeeEditor = employeeEditor;
	}
	public String getEmployeeEditDate() {
		return EmployeeEditDate;
	}
	public void setEmployeeEditDate(String employeeEditDate) {
		EmployeeEditDate = employeeEditDate;
	}
}

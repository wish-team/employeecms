package bean;

/*
 *管理者情報・マスタデータ
 */

public class Manager implements java.io.Serializable {

	private int id;
	private String idStr;
	private String fullname;
	private String username;
	private String password;
	private String departmentName;
	private String director;
	private String databaseName;
	private String industryName;
	private String languageName;
	private String frameworkName;
	private String certificateName;
	private String editUrl;


	public String getEditUrl() {
		return editUrl;
	}
	public void setEditUrl(String editUrl) {
		this.editUrl = editUrl;
	}
	public String getCertificateName() {
		return certificateName;
	}
	public void setCertificateName(String certificateName) {
		this.certificateName = certificateName;
	}
	//StringのID（業種とFWのID）
	public String getIdStr() {
		return idStr;
	}
	public void setIdStr(String idStr) {
		this.idStr = idStr;
	}

	//部署名のセッターとゲッター
	public String getDepartmentName() {
		return departmentName;
	}
	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}

	//部長のセッターとゲッター
	public String getDirector() {
		return director;
	}
	public void setDirector(String director) {
		this.director = director;
	}

	//DB名のセッターとゲッター
	public String getDatabaseName() {
		return databaseName;
	}
	public void setDatabaseName(String databaseName) {
		this.databaseName = databaseName;
	}

	//業種のセッターとゲッター
	public String getIndustryName() {
		return industryName;
	}
	public void setIndustryName(String industryName) {
		this.industryName = industryName;
	}

	//言語のセッターとゲッター
	public String getLanguageName() {
		return languageName;
	}
	public void setLanguageName(String languageName) {
		this.languageName = languageName;
	}

	//FWのセッターとゲッター
	public String getFrameworkName() {
		return frameworkName;
	}
	public void setFrameworkName(String frameworkName) {
		this.frameworkName = frameworkName;
	}

	//管理者IDのセッターとゲッター
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}

	//管理者氏名のセッターとゲッター
	public String getFullname() {
		return fullname;
	}
	public void setFullname(String fullname) {
		this.fullname = fullname;
	}

	//ログインIDのセッターとゲッター
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}

	//パスワードのセッターとゲッター
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}

}

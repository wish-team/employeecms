package bean;

public class Error implements java.io.Serializable {

	private String error;

	public String getError() {
		return error;
	}

	public String setError(String error) {
		this.error = "<p class='error'>" + error + "</p>";
		return this.error;
	}
}


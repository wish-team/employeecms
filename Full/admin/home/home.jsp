<%@page contentType="text/html; charset=UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Wish Team Project</title>
</head>

<style>
	<%@include file="/css/reset.css"%>
	<%@include file="/css/global.css"%>
	<%@include file="/css/style.css"%>
</style>

<body>
	<div id="wrapper">
        <div class="wp-inner">
            <div id="content">
                <div class="page-title clearfix">
                    <p class="fl-left">社員情報管理システム</p>

                    <div class="manager-info fl-right">
                    	<div class="manager-name">
                    		<span>
								<c:choose>
                    				<c:when test="${isLogin == true }">
                    					${username}
                    				</c:when>
                    			</c:choose>
                			</span>
                    	</div>

						<div id="manager-logout">
							  <a href="../manager/Logout.action"> ( ログアウト )</a>
						</div>
					</div>
                </div>
                <div class="page-notice">
                    <p>実行するメニューボタンを選択してください</p>
                </div>

                <div class="page-content">
                    <div class="content-up">
                        <a href="../user/AddUserForm.action">社員登録</a>
                        <a href="../manager/AddAdminForm.action">管理者登録</a>
                    </div>

                    <div class="content-down">
                        <a href="../user/ListUser.action">社員情報一覧</a>
                        <a href="../search/Search.action">社員情報検索</a>
                        <a href="../manager/Master.action">元帳管理</a>
                    </div>

                </div>
            </div>
        </div>
    </div>
</body>
</html>
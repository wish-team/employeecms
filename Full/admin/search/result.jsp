<%@page contentType="text/html; charset=UTF-8"%>
<%@include file="/header.jsp"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div id="content">
	<div class="table-title">
		<h3>検索結果</h3>
	</div>

	<div class="table-content">
		<c:choose>
			<c:when test="${(res != 200)}">
				<p>検索対象が見つかりません</p>
			</c:when>

			<c:when test="${(res == 200)}">
				<table class="table">
					<thead>
						<tr>
							<td><span class="thead-text">氏名</span></td>
							<td><span class="thead-text">性別</span></td>
							<td><span class="thead-text">生年月日</span></td>
							<td><span class="thead-text">年齢</span></td>
							<td><span class="thead-text">経験年数</span></td>
							<td><span class="thead-text">所属部署</span></td>
							<td><span class="thead-text">資格</span></td>
						</tr>
					</thead>
					<tbody>
						<c:forEach var="item" items="${listUser}">
							<tr>
								<td class=""><a href="${item.getDetailUrl()}" title="">${item.getFullname()}</a></td>
								<td>
									<span class="tbody-text">
										<c:choose>
											<c:when test="${item.getGender() == 0}">男</c:when>
											<c:when test="${item.getGender() == 1}">女</c:when>
										</c:choose>
									</span>
								</td>
								<td><span class="tbody-text">${item.getBirth()}</span></td>
								<td><span class="tbody-text">${item.getAge()}</span></td>
								<td><span class="tbody-text">${item.getExperience()}</span></td>
								<td><span class="tbody-text">${item.getDepartmentName()}</span></td>
								<td>
									<c:if test="${item.getListCertificate() != null}">
										<c:forEach var="cer" items="${item.getListCertificate()}">
											<p class="tbody-text">${cer.getCertificateName()}</p>
										</c:forEach>
									</c:if>
								</td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</c:when>
		</c:choose>
	</div>
</div>

<%@include file="/footer.html"%>

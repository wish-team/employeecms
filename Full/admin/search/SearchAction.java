package admin.search;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import bean.Error;
import bean.Manager;
import bean.User;
import bean.Validation;
import dao.ManagerDAO;
import dao.UserDAO;
import tool.Action;

public class SearchAction extends Action {
	public String execute(HttpServletRequest request, HttpServletResponse response) throws Exception {

		//		HttpSession session = request.getSession();
		UserDAO userDao = new UserDAO();
		ManagerDAO managerDao = new ManagerDAO();
		Validation form = new Validation();
		Error error = new Error();
		String btn = request.getParameter("btn-submit");

		if (btn == null) btn = "";
		if ((!btn.isEmpty()) || btn.equals("submit")) {
			String fullname = request.getParameter("fullname");
			String diplomaOp1 = request.getParameter("diplomaOp1");
			String diplomaOp2 = request.getParameter("diplomaOp2");
			String diplomaOp3 = request.getParameter("diplomaOp3");
			String age = request.getParameter("age");
			age = age.replace("0", "");
			String exStart = request.getParameter("exStart");
			String exEnd = request.getParameter("exEnd");

			if(form.validationSearchForm(fullname, diplomaOp1, diplomaOp2, diplomaOp3, age, exStart, exEnd)) {
				List<User> listId = userDao.search(fullname, diplomaOp1, diplomaOp2, diplomaOp3, age, exStart, exEnd);
				List<User> listUser = new ArrayList<>();
				HashMap<String, String> where = new HashMap<String, String>();

				for (User item : listId) {
					String id = Integer.toString(item.getUserId());
					where.put("us.id", id);
					listUser.addAll(userDao.dbGetData("select * from tbl_user as us join tbl_department as dp on us.department = dp.id", where, "user"));
				}

				for (User item : listUser) 	{
					HashMap<String, String> whereCer = new HashMap<String, String>();
					String id = Integer.toString(item.getUserId());
					whereCer.put("userId", id);
					List<User> listCertificate = userDao.dbGetData("select * from tbl_certificate_detail as dt INNER JOIN tbl_certificate as c ON dt.certificateId = c.id", whereCer, "listCertificate");
					item.setListCertificate(listCertificate);
					item.setDetailUrl("../user/Detail.action?id=" + item.getUserId());
				}

				if((listUser.size() > 0) && (listUser != null)) {
					request.setAttribute("res", 200);
					request.setAttribute("listUser", listUser);
				}
				return "result.jsp";
			}
			error.setError("最低1つの検索条件を入力してください");
			request.setAttribute("error", error);
		}

		List<Manager> listCertificate = managerDao.dbGetData("select * from tbl_certificate", null, "certificate");
		request.setAttribute("listCertificate", listCertificate);
		return "search.jsp";
	}

}

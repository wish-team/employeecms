<%@page contentType="text/html; charset=UTF-8"%>
<%@include file="/header.jsp"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div id="content">
	<div class="search-form-title">
		<h3>社員情報検索</h3>
	</div>

	<div class="search-form-content">
		<div class="search-form-wp">
			<form action="" method="get" id="search-form">
				<div class="widget clearfix">
					<div class="fill-label fl-left">
						<label for="fullname">氏名:</label>
					</div>

					<div class="fill-info">
						<input type="text" name="fullname" id="fullname" value=""
							placeholder="氏名入力">
					</div>
				</div>

				<div class="widget clearfix">
					<div class="fill-label fl-left">
						<label for="diplomaOp1">所有資格:</label>
					</div>

					<div class="fill-info">
						<br> <select name="diplomaOp1" id="diplomaOp1">
							<option value="">----</option>
							<c:forEach var="item" items="${listCertificate}">
								<option value="${item.getId()}">${item.getCertificateName()}</option>
							</c:forEach>
						</select> <span>資格1</span> <select name="diplomaOp2" id="diplomaOp2">
							<option value="">----</option>
							<c:forEach var="item" items="${listCertificate}">
								<option value="${item.getId()}">${item.getCertificateName()}</option>
							</c:forEach>
						</select> <span>資格2</span> <select name="diplomaOp3" id="diplomaOp3">
							<option value="">----</option>
							<c:forEach var="item" items="${listCertificate}">
								<option value="${item.getId()}">${item.getCertificateName()}</option>
							</c:forEach>
						</select> <span>資格3</span>
					</div>
				</div>

				<div class="widget clearfix">
					<div class="fill-label fl-left">
						<label for="age">年代:</label>
					</div>

					<div class="fill-info">
						<select name="age" id="age">
							<option value="">----</option>
							<c:forEach var="i" begin="10" end="60" step="10">
								<option value="${i}">${i}代</option>
							</c:forEach>
						</select>
					</div>
				</div>

				<div class="widget clearfix">
					<div class="fill-label fl-left">
						<label for="exStart">経験年数:</label>
					</div>

					<div class="fill-info">
						<select name="exStart" id="exStart">
							<option value="">----</option>
							<c:forEach var="i" begin="1" end="20" step="1">
								<option value="${i}">${i}</option>
							</c:forEach>
						</select><span>~</span><select name="exEnd" id="exEnd">
							<option value="">----</option>
							<c:forEach var="i" begin="1" end="20" step="1">
								<option value="${i}">${i}</option>
							</c:forEach>
						</select> <span>年</span>

					</div>
				</div>
				<button type="submit" name="btn-submit" value="submit">検索</button>
				<c:choose>
					<c:when test="${error != null}">
							${error.getError()}
						</c:when>
				</c:choose>
			</form>
		</div>
	</div>
</div>

<%@include file="/footer.html"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@include file="/header.jsp"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div id="content">

	<div class="add-database">
		<form action="AddDatabase.action" method="post" id="add-database-form">

				<input type="text" name="databaseName" placeholder="DB名" id="database-name">
				<c:choose>
					<c:when  test="${error != null}">
						${error.getError()}
					</c:when>
				</c:choose>
			<div class="insert-button">
				<p><input type="submit" value="追加"></p>
				<p><input type="submit" value="キャンセル"></p>
			</div>



		</form>
	</div>
</div>
<%@include file="/footer.html"%>
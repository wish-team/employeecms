<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@include file="/header.jsp"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div id="content">

	<div class="add-framework">
		<form action="AddFramework.action" method="post"
			id="add-framework-from">

			<div id="framework-input">
				<input type="text" name="id" placeholder="ID" id="framework-id">
				<input type="text" name="frameworkName" placeholder="FW名"
					id="framework-name">
			</div>
			<c:choose>
				<c:when test="${error != null}">
						${error.getError()}
					</c:when>
			</c:choose>
			<div class="insert-button">
				<p>
					<input type="submit" value="追加">
				</p>
				<p>
					<input type="submit" value="キャンセル">
				</p>
			</div>
		</form>
	</div>
</div>
<%@include file="/footer.html"%>
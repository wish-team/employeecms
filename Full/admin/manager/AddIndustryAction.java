package admin.manager;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import bean.Error;
import bean.Helper;
import dao.ManagerDAO;
import tool.Action;

public class AddIndustryAction extends Action {
//	@SuppressWarnings("unchecked")
	public String execute(HttpServletRequest request, HttpServletResponse response) throws Exception {

		HashMap<String, String> map = new HashMap<String, String>();
		Error er = new Error();
		String id = request.getParameter("id");
		String industryName = request.getParameter("industryName");
		Helper help = new Helper();

		if (id.isEmpty()) {
			er.setError("IDを入力してください");
			request.setAttribute("error", er);
			return "addIndustry.jsp";
		}

		if (industryName.isEmpty()) {
			er.setError("業種名を入力してください");
			request.setAttribute("error", er);
			return "addIndustry.jsp";
		}

		if (!help.isNormalText(id)) {
			er.setError("半角で入力してください");
			request.setAttribute("error", er);
			return "addFramework.jsp";
		}

		map.put("id", id);
		map.put("industryName", industryName);

		ManagerDAO dao=new ManagerDAO();

		/*
		 *ID・業種名の検索と比較
		 */
		if(dao.isExist("tbl_industry", "id",id)){
			er.setError("すでに存在しているIDです。");
			request.setAttribute("error", er);
			return "addIndustry.jsp";
		}

		if(dao.isExist("tbl_industry", "industryName",industryName)){
				er.setError("すでに存在している業種名です。");
				request.setAttribute("error", er);
				return "addIndustry.jsp";
		}

		dao.dbInsert("tbl_industry", map,2);
		return "showMaster.jsp";
	}
}

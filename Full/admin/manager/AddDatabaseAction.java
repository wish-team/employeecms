package admin.manager;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import bean.Error;
import dao.ManagerDAO;
import tool.Action;
public class AddDatabaseAction extends Action {
//	@SuppressWarnings("unchecked")
	public String execute(HttpServletRequest request, HttpServletResponse response) throws Exception {

		HashMap<String, String> map = new HashMap<String, String>();
		Error er = new Error();
		String databaseName = request.getParameter("databaseName");

		map.put("id", null);
		map.put("databasename", databaseName);

		ManagerDAO dao=new ManagerDAO();
		if(dao.isExist("tbl_database", "databaseName", databaseName)) {
			er.setError("存在");
			request.setAttribute("error", er);
			return "addDatabase.jsp";
		}
		dao.dbInsert("tbl_database", map, 1);
		return "showMaster.jsp";
	}


}

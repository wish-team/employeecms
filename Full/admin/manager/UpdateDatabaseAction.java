package admin.manager;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import bean.Error;
import dao.ManagerDAO;
import tool.Action;

public class UpdateDatabaseAction extends Action{

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) throws Exception {

		Error er = new Error();

		int id = Integer.parseInt(request.getParameter("id"));
		String databaseName = request.getParameter("databaseName");
		databaseName = databaseName.trim();

		//空文字判定
		if (databaseName.isEmpty()) {
			er.setError("DB名を入れてください");
			request.setAttribute("error", er);
			return "editDatabase.jsp";
		}

		ManagerDAO dao = new ManagerDAO();

		if(dao.isExist("tbl_database", "databaseName",databaseName)){
			er.setError("すでに存在しているDB名です。");
			request.setAttribute("error", er);
			return "editDatabase.jsp";
		}

		//daoのupdate処理を呼ぶ
		dao.dbUpdate("tbl_database","databasename",id, databaseName);


		return "showMaster.jsp";
	}

}

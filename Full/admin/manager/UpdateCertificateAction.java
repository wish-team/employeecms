package admin.manager;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import bean.Error;
import dao.ManagerDAO;
import tool.Action;
public class UpdateCertificateAction extends Action {
//	@SuppressWarnings("unchecked")
	public String execute(HttpServletRequest request, HttpServletResponse response) throws Exception {

		Error er = new Error();

		int id = Integer.parseInt(request.getParameter("id"));
		String certificateName = request.getParameter("certificateName");
		certificateName = certificateName.trim();

		//空文字判定
		if(certificateName.isEmpty()) {
			er.setError("資格名を入れてください");
			request.setAttribute("error", er);
			return "addCertificate.jsp";
		}

		ManagerDAO dao=new ManagerDAO();

		if(dao.isExist("tbl_certificate", "certificateName", certificateName)) {
			er.setError("存在");
			request.setAttribute("error", er);
			return "addCertificate.jsp";
		}

		//daoのupdate処理を呼ぶ
		dao.dbUpdate("tbl_certificate", "certificatename",id,certificateName);

		return "showMaster.jsp";
	}


}

<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@include file="/header.jsp"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<div id="content">

	<div class="add-industry">
		<form action="AddIndustry.action" method="post" id="add-industry-from">

			<div id="industry-input">
				<input type="text" name="id" placeholder="ID" id="industry-id">
				<input type="text" name="industryName" placeholder="業種名"
					id="industry-name">
				<c:choose>
					<c:when test="${error != null}">
						${error.getError()}
					</c:when>
				</c:choose>
			</div>

			<div class="insert-button">
				<p>
					<input type="submit" value="追加">
				</p>
				<p>
					<input type="submit" value="キャンセル">
				</p>
			</div>


		</form>
	</div>
</div>

<%@include file="/footer.html"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@include file="/header.jsp"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div id="content">

	<div class="add-department">
		<form action="AddDepartment.action" method="post" id="add-department-form">

	<div id="department-input">
				<input type="text" name="departmentName" value="" placeholder="部署名" id="department-id">
				<input type="text" name="director" value="" placeholder="部長" id="department-name">
				<c:choose>
					<c:when test="${error != null}">
						${error.getError()}
					</c:when>
				</c:choose>
	</div>

			<div class="insert-button">
				<p><input type="submit" value="追加"></p>
				<p><input type="submit" value="キャンセル"></p>
			</div>
		</form>
	</div>
</div>
<%@include file="/footer.html"%>
<%@page contentType="text/html; charset=UTF-8"%>
<%@include file="/header.jsp"%>

<div class="table-title">
	<div class="adduser-form">
		<h3>管理者登録画面</h3>
	</div>
</div>


<div class="search-form-content">
	<div class="search-form-wp">
		<form action="AddAdmin.action" method="get" id="search-form">
			<div class="widget clearfix">
				<div class="fill-label fl-left">
					<label for="fullname">氏名:</label>
				</div>

				<div class="fill-info">
					<input type="text" name="fullname" id="fullname" value=""
						placeholder="氏名入力">
				</div>
			</div>
			<div class="widget clearfix">
				<div class="fill-label fl-left">
					<label for="fullname">ログインID:</label>

				</div>

				<div class="fill-info">
					<input type="text" name="username" id="username" value=""
						placeholder="ID入力">
				</div>
			</div>
			<div class="widget clearfix">
				<div class="fill-label fl-left">
					<label for="fullname">パスワード:</label>
				</div>

				<div class="fill-info">
					<input type="password" name="password" id="password" value=""
						placeholder="パスワード入力">
				</div>
			</div>
			<div class="widget clearfix">
				<div class="fill-label fl-left">
					<label for="fullname">パスワード:</label>
				</div>

				<div class="fill-info">
					<input type="password" name="subpassword" id="subpassword" value=""
						placeholder="パスワード確認">
				</div>
			</div>

			<button type="submit" name="btn-submit" value="submit">登録</button>
		</form>
	</div>
</div>
<%@ include file="/footer.html"%>
package admin.manager;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import bean.Error;
import bean.Manager;
import dao.ManagerDAO;
import tool.Action;

/*
 * 管理者ログイン
 * abe yasuko
 */
public class LoginAction extends Action{

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		//セッションの開始

		Error er = new Error();

		//パラメータの取得（ログインIDとパスワード）
		String username = request.getParameter("username");
		String password = request.getParameter("password");

		//入力値が空の場合の判定処理
		if(username.isEmpty() || password.isEmpty()) {
			er.setError("IDもしくはPASSが入力されていません。");
			request.setAttribute("error", er);
			return "login.jsp";
		}

		//ログインIDが同じものがあった場合の判定処理

		//取得したパラメートをDAOのsearchメソッドを使用して検索
		ManagerDAO dao = new ManagerDAO();
		Manager manager = dao.login(username,password);

		//ログイン成功の処理
		if(manager != null) {
			HttpSession session = request.getSession();
			session.setAttribute("isLogin", true);
			session.setAttribute("username", username);
			return "../home/Home.action";
		}

		//ログイン失敗の処理
		return "../manager/login.jsp";
	}

}

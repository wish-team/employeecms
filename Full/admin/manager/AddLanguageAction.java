package admin.manager;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import bean.Error;
import dao.ManagerDAO;
import tool.Action;

public class AddLanguageAction extends Action {
//	@SuppressWarnings("unchecked")
	public String execute(HttpServletRequest request, HttpServletResponse response) throws Exception {

		HashMap<String, String> map = new HashMap<String, String>();
		Error er = new Error();
		String languageName = request.getParameter("languageName");

		map.put("id", null);
		map.put("languageName", languageName);

		ManagerDAO dao=new ManagerDAO();

		/*
		 *言語名の検索と比較
		 */

		if(dao.isExist("tbl_language", "languageName",languageName)){
				er.setError("すでに存在している言語名です。");
				request.setAttribute("error", er);
				return "addLanguage.jsp";
		}

		dao.dbInsert("tbl_language", map,1);
		return "showMaster.jsp";
	}
}

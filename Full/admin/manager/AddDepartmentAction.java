package admin.manager;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import bean.Error;
import dao.ManagerDAO;
import tool.Action;

public class AddDepartmentAction extends Action {
//	@SuppressWarnings("unchecked")
	public String execute(HttpServletRequest request, HttpServletResponse response) throws Exception {

		HashMap<String, String> map = new HashMap<String, String>();
		Error er = new Error();

		String departmentName = request.getParameter("departmentName");
		String director= request.getParameter("director");

		if (departmentName.isEmpty()) {
			er.setError("部署名を入力してください");
			request.setAttribute("error", er);
			return "addDepartment.jsp";
		}

		map.put("departmentName", departmentName);
		map.put("director", director);

		ManagerDAO dao=new ManagerDAO();

		/*
		 *部署名・部長の検索と比較
		 */
		if(dao.isExist("tbl_department", "departmentName",departmentName)){
			er.setError("すでに存在している部署名です。");
			request.setAttribute("error", er);
			return "addDepartment.jsp";
		}

		dao.dbInsert("tbl_department", map,1);
		return "showMaster.jsp";
	}
}

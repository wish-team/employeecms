package admin.user;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import bean.User;
import dao.UserDAO;
import tool.Action;

public class EditUserWorkHistoryAction extends Action {
	public String execute(
			HttpServletRequest request, HttpServletResponse response) throws Exception {

		String workHistoryId = request.getParameter("workHistoryId");
		UserDAO dao = new UserDAO();

		String yearStart = request.getParameter("yearStart");
		String mounthStart = request.getParameter("mounthStart");//
		String dayStart = request.getParameter("dayStart");
		String startTime = yearStart + "-" + mounthStart + "-" + dayStart;

		String yearEnd = request.getParameter("yearEnd");
		String mounthEnd = request.getParameter("mounthEnd");
		String dayEnd = request.getParameter("dayEnd");
		String endTime = yearEnd + "-" + mounthEnd + "-" + dayEnd;

		String industry = request.getParameter("industry");
		String detail = request.getParameter("detail");

		HashMap<String, String> dataWork = new HashMap<String, String>();
		dataWork.put("startTime", startTime);
		dataWork.put("endTime", endTime);
		if(!industry.isEmpty()) dataWork.put("industryId", industry);
		dataWork.put("detail", detail);

		HashMap<String, String> whereWork = new HashMap<String, String>();
		whereWork.put("workHistoryId", workHistoryId);

		dao.dbUpdate("tbl_work_history", dataWork, null, whereWork, 0);

		ArrayList<String> listLanguage = new ArrayList<String>();
		String languageOp1 = request.getParameter("languageOp1");
		if (languageOp1 == null) languageOp1 = "";
		String languageOp2 = request.getParameter("languageOp2");
		if (languageOp2 == null) languageOp2 = "";
		String languageOp3 = request.getParameter("languageOp3");
		if (languageOp3 == null) languageOp3 = "";

		ArrayList<String> listDatabase = new ArrayList<String>();
		String databaseOp1 = request.getParameter("databaseOp1");
		if (databaseOp1 == null) databaseOp1 = "";
		String databaseOp2 = request.getParameter("databaseOp2");
		if (databaseOp2 == null) databaseOp2 = "";
		String databaseOp3 = request.getParameter("databaseOp3");
		if (databaseOp3 == null) databaseOp3 = "";

		ArrayList<String> listFramework = new ArrayList<String>();
		String frameworkOp1 = request.getParameter("frameworkOp1");
		if (frameworkOp1 == null) frameworkOp1 = "";
		String frameworkOp2 = request.getParameter("frameworkOp2");
		if (frameworkOp2 == null) frameworkOp2 = "";
		String frameworkOp3 = request.getParameter("frameworkOp3");
		if (frameworkOp3 == null) frameworkOp3 = "";

		if (!languageOp1.isEmpty()) listLanguage.add(languageOp1);
		if (languageOp1.isEmpty()) listLanguage.add("");
		if (!languageOp2.isEmpty())listLanguage.add(languageOp2);
		if (languageOp2.isEmpty()) listLanguage.add("");
		if (!languageOp3.isEmpty())listLanguage.add(languageOp3);
		if (languageOp3.isEmpty())listLanguage.add("");

		if (!databaseOp1.isEmpty()) listDatabase.add(databaseOp1);
		if (databaseOp1.isEmpty()) listDatabase.add("");
		if (!databaseOp2.isEmpty())listDatabase.add(databaseOp2);
		if (databaseOp2.isEmpty()) listDatabase.add("");
		if (!databaseOp3.isEmpty())listDatabase.add(databaseOp3);
		if (databaseOp3.isEmpty()) listDatabase.add("");

		if (!frameworkOp1.isEmpty())listFramework.add(frameworkOp1);
		if (frameworkOp1.isEmpty()) listFramework.add("");
		if (!frameworkOp2.isEmpty()) listFramework.add(frameworkOp2);
		if (frameworkOp2.isEmpty())listFramework.add("");
		if (!frameworkOp3.isEmpty())listFramework.add(frameworkOp3);
		if (frameworkOp3.isEmpty())listFramework.add("");

		HashMap<String, String> whereDetail = new HashMap<String, String>();
		whereDetail.put("workHistoryId", workHistoryId);
		List<User> listWorkHistoryDetail = dao.dbGetData("select * from tbl_work_history_detail", whereDetail,
				"workHistoryDetailById");

		int i = 0;
		if (listLanguage.size() > 0) {
			for (User item : listWorkHistoryDetail) {
				HashMap<String, String> whereUpdateDetail = new HashMap<String, String>();
				HashMap<String, Integer> data = new HashMap<String, Integer>();
				String id = Integer.toString(item.getId());
				whereUpdateDetail.put("id", id);
				whereUpdateDetail.put("workHistoryId", workHistoryId);

				if (listLanguage.get(i) != null && !listLanguage.get(i).isEmpty()) {
					int value = Integer.parseInt(listLanguage.get(i));
					data.put("languageId", value);
				} else {
					data.put("languageId", 0);
				}
				dao.dbUpdate("tbl_work_history_detail", null, data, whereUpdateDetail, 1);
				i++;
			}
		}

		i = 0;
		if (listDatabase.size() > 0) {
			for (User item : listWorkHistoryDetail) {
				HashMap<String, String> whereUpdateDetail = new HashMap<String, String>();
				HashMap<String, Integer> data = new HashMap<String, Integer>();
				String id = Integer.toString(item.getId());
				whereUpdateDetail.put("id", id);
				whereUpdateDetail.put("workHistoryId", workHistoryId);

				if (listDatabase.get(i) != null && !listDatabase.get(i).isEmpty()) {
					int value = Integer.parseInt(listDatabase.get(i));
					data.put("databaseId", value);
				} else {
					data.put("databaseId", 0);
				}

				dao.dbUpdate("tbl_work_history_detail", null, data, whereUpdateDetail, 1);
				i++;
			}
		}

		i=0;
		if (listFramework.size() > 0) {
			for (User item : listWorkHistoryDetail) {
				HashMap<String, String> whereUpdateDetail = new HashMap<String, String>();
				HashMap<String, String> data = new HashMap<String, String>();
				String id = Integer.toString(item.getId());
				whereUpdateDetail.put("id", id);
				whereUpdateDetail.put("workHistoryId", workHistoryId);

				if (listFramework.get(i) != null && !listFramework.get(i).isEmpty()) {
					data.put("frameworkId", listFramework.get(i));
				} else {
					data.put("frameworkId", null);
				}

				dao.dbUpdate("tbl_work_history_detail", data, null, whereUpdateDetail, 0);
				i++;
			}
		}
		return "../user/ListUser.action";
	}

}

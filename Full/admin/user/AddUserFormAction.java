package admin.user;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import bean.Manager;
import dao.ManagerDAO;
import tool.Action;

public class AddUserFormAction extends Action {
	//	@SuppressWarnings("unchecked")
	public String execute(HttpServletRequest request, HttpServletResponse response) throws Exception {

		//ユーザーのIDを取得
		ManagerDAO dao = new ManagerDAO();

//		String btn = request.getParameter("btn-submit");
//		if(btn == null) btn = "";
//		if((!btn.isEmpty()) || btn.equals("submit")) {

		List<Manager> listDepartment = new ArrayList<Manager>();
		List<Manager> listCertificate = new ArrayList<Manager>();

		listDepartment = dao.dbGetData("select * from tbl_department",null,"department");
		request.setAttribute("listDepartment", listDepartment);

		listCertificate = dao.dbGetData("select * from tbl_certificate",null,"certificate");
		request.setAttribute("listCertificate", listCertificate);

		return "../user/addUser.jsp";

//		}

//		return "/admin/home/home.jsp";
	}
}

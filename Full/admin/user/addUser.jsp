<%@page contentType="text/html; charset=UTF-8"%>
<%@include file="/header.jsp"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


<div class="table-title">
	<div class="adduser-form">
		<h3>社員登録画面</h3>
	</div>
	<br>
	<div class="search-form-content">
		<div class="search-form-wp">
			<form action="AddUser.action" method="get" id="search-form">
				<div class="widget clearfix">
					<div class="fill-label fl-left">
						<label for="fullname">氏名:</label>
					</div>

					<div class="fill-info">
						<input type="text" name="fullname" id="fullname" value=""
							placeholder="氏名入力">
					</div>
				</div>
				<div class="widget clearfix">
					<div class="fill-label fl-left">
						<label for="username">ログインID:</label>
					</div>

					<div class="fill-info">
						<input type="text" name="new_username" id="new_username" value=""
							placeholder="ログインID入力">
					</div>
				</div>
				<div class="widget clearfix">
					<div class="fill-label fl-left">
						<label for="password">パスワード:</label>
					</div>
					<div class="fill-info">
						<input type="password" name="password" id="password" value=""
							placeholder="パスワード入力">
					</div>
				</div>
				<div class="widget clearfix">
					<div class="fill-label fl-left">
						<label for="fullname">パスワード:</label>
					</div>

					<div class="fill-info">
						<input type="password" name="subpassword" id="subpassword" value=""
							placeholder="パスワード確認">
					</div>
				</div>
				<div class="widget clearfix">
					<div class="fill-label fl-left">
						<label for="sex">性別:</label>
					</div>
					男<input type="radio" name="sex" value="0"> 女<input
						type="radio" name="sex" value="1">
				</div>
				<div class="widget clearfix">
					<div class="fill-label fl-left">
						<label for="age">生年月日:</label>
					</div>


					<div class="fill-info">
						<select name="year" id="year">
							<option value="">-</option>
							<%
							for (int i = 1900; i <= 2022; i++) {
							%>
							<option value="<%=i%>"><%=i%></option>
							<%
							}
							%>
						</select><span>年</span> <select name="month" id="month">
							<option value="">-</option>
							<%
							for (int j = 1; j <= 12; j++) {
							%>
							<option value="<%=j%>"><%=j%></option>
							<%
							}
							%>
						</select><span>月</span><select name="day" id="day">
							<option value="">-</option>
							<%
							for (int k = 1; k <= 31; k++) {
							%>
							<option value="<%=k%>"><%=k%></option>
							<%
							}
							%>
						</select><span>日</span>
					</div>
				</div>

				<div class="widget clearfix">
					<div class="fill-label fl-left">
						<label for="experience">経験年数:</label>
					</div>

					<select name="experience" id="experience">
						<option value="">-</option>
						<%
						for (int l = 0; l <= 99; l++) {
						%>
						<option value="<%=l%>"><%=l%></option>
						<%
						}
						%>
					</select>年
				</div>

				<div class="widget clearfix">
					<div class="fill-label fl-left">
						<label for="department_id">部署:</label>
					</div>
					<select name="department_id">
						<option value="">--</option>
						<c:forEach var="item" items="${listDepartment}">
							<option value="${item.getId()}">${item.getDepartmentName()}</option>
						</c:forEach>
					</select>
				</div>

				<div class="widget clearfix">
					<div class="fill-label fl-left">
						<label for="certificate_id">取得資格1:</label>
					</div>
					<select name="certificate_id1">
						<option value="0">--</option>
						<c:forEach var="item" items="${listCertificate}">
							<option value="${item.getId()}">${item.getCertificateName()}</option>
						</c:forEach>
					</select><br>
					<div class="fill-label fl-left">
						<label for="certificate_id">取得資格2:</label>
					</div>
					<select name="certificate_id2">
						<option value="0">--</option>
						<c:forEach var="item" items="${listCertificate}">
							<option value="${item.getId()}">${item.getCertificateName()}</option>
						</c:forEach>
					</select><br>
					<div class="fill-label fl-left">
						<label for="certificate_id">取得資格3:</label>
					</div>
					<select name="certificate_id3">
						<option value="0">--</option>
						<c:forEach var="item" items="${listCertificate}">
							<option value="${item.getId()}">${item.getCertificateName()}</option>
						</c:forEach>
					</select>

				</div>


				<button type="submit" name="btn-submit" value="submit">登録</button>
			</form>
		</div>
	</div>
</div>
<%@ include file="/footer.html"%>
package admin.user;

import java.util.Calendar;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import bean.Error;
import bean.User;
import dao.UserDAO;
import tool.Action;

public class AddUserAction extends Action {
	public String execute(
			HttpServletRequest request, HttpServletResponse response) throws Exception {

		Error er = new Error();
		HttpSession session = request.getSession();

		//パラメーターより値をそれぞれ取得
		String fullname = request.getParameter("fullname");
		int sex = Integer.parseInt(request.getParameter("sex"));
		String year = request.getParameter("year");
		String month = request.getParameter("month");
		String day = request.getParameter("day");
		String strDate = year + "-" + month + "-" + day;
		Date birth = java.sql.Date.valueOf(strDate);
		int age = calcAge(birth);
		int experience = Integer.parseInt(request.getParameter("experience"));
		int department_id = Integer.parseInt(request.getParameter("department_id"));
		int[] certificate_ids = { Integer.parseInt(request.getParameter("certificate_id1")),
				Integer.parseInt(request.getParameter("certificate_id2")),
				Integer.parseInt(request.getParameter("certificate_id3")) };
		String creator = (String) session.getAttribute("username");
		String username = request.getParameter("new_username");
		String password = request.getParameter("password");
		String subpassword = request.getParameter("subpassword");

		if (password.equals(subpassword)) {

			//beanを生成し、代入する
			User u = new User();
			u.setFullname(fullname);
			u.setGender(sex);
			u.setBirth_date(birth);
			u.setAge(age);
			u.setExperience(experience);
			u.setDepartmentId(department_id);
			u.setCertificate_ids(certificate_ids);
			u.setCreator(creator);
			u.setUsername(username);
			u.setPassword(password);
			u.setCreator(creator);

			//DAOに入り登録処理を実行
			UserDAO dao = new UserDAO();
			int line = dao.insert(u);

			//正しく更新できた場合ホーム画面へ遷移する。
			if (line > 0) {
				return "../home/home.jsp";//

				//ただしく更新できなかった場合エラーページに遷移させる
			} else {
				er.setError("未入力、もしくは正しく入力されていない箇所があります。");
				request.setAttribute("error", er);
				return "addUser.jsp";
			}

		} else {
			er.setError("未入力、もしくは正しく入力されていない箇所があります。");
			request.setAttribute("error", er);
			return "addUser.jsp";
		}
	}

	//誕生日から年齢を計算するメソッド
	public int calcAge(Date birth) {

		// Calendar型のインスタンス生成
		Calendar calendarBirth = Calendar.getInstance();
		Calendar calendarNow = Calendar.getInstance();

		//現在日時の取得
		Date now = new Date();

		// Date型をCalendar型に変換する
		calendarBirth.setTime(birth);
		calendarNow.setTime(now);

		// （現在年ー生まれ年）で年齢の計算
		int age = calendarNow.get(Calendar.YEAR) - calendarBirth.get(Calendar.YEAR);
		// 誕生月を迎えていなければ年齢-1
		if (calendarNow.get(Calendar.MONTH) < calendarBirth.get(Calendar.MONTH)) {
			age -= 1;
		} else if (calendarNow.get(Calendar.MONTH) == calendarBirth.get(Calendar.MONTH)) {
			// 誕生月は迎えているが、誕生日を迎えていなければ年齢−１
			if (calendarNow.get(Calendar.DATE) < calendarBirth.get(Calendar.DATE)) {
				age -= 1;
			}
		}

		return age;
	}

}
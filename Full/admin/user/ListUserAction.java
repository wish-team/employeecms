package admin.user;

import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import bean.Render;
import bean.User;
import dao.UserDAO;
import tool.Action;

public class ListUserAction extends Action {
//	@SuppressWarnings("unchecked")
	public String execute(HttpServletRequest request, HttpServletResponse response) throws Exception {

//		HttpSession session = request.getSession();
		UserDAO dao = new UserDAO();
		HashMap<String, String> whereStatus = new HashMap<String, String>();
		Render render = new Render();
		whereStatus.put("status", "0");
		List<User> dbNumRow = dao.dbGetData("select * from (tbl_user as us INNER JOIN tbl_department as dp ON us.department = dp.id ) INNER JOIN TBL_ADMIN as ad ON us.creator = ad.id", whereStatus, "user");

		//Pagging 計算
		double numPerPage = 2;
		double numRow = dbNumRow.size();
		double numPage = Math.ceil(numRow / numPerPage);
		String page = request.getParameter("page");
		if(page == null ) page = "1";
		double start = (Integer.parseInt(page) - 1) * numPerPage;

		List<User> listUser = dao.dbGetData("select * from (tbl_user as us INNER JOIN tbl_department as dp ON us.department = dp.id ) INNER JOIN TBL_ADMIN as ad ON us.creator = ad.id limit " + start + ","+ numPerPage, null, "user");
		for(User item : listUser) {
			String id = Integer.toString(item.getUserId());
			HashMap<String, String> where = new HashMap<String, String>();
			where.put("userId", id);
			List<User> listCertificate = dao.dbGetData("select * from tbl_certificate_detail as dt INNER JOIN tbl_certificate as c ON dt.certificateId = c.id", where, "listCertificate");
			item.setListCertificate(listCertificate);
			item.setDetailUrl("../user/Detail.action?id=" + id);
		}

		render.setPagging(numPage , Integer.parseInt(page), "../user/ListUser.action");

		request.setAttribute("listUser", listUser);
		request.setAttribute("pagging", render);
		return "listUser.jsp";
	}
}

<%@page contentType="text/html; charset=UTF-8"%>
<%@include file="/header.jsp"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<div id="content">
	<div class="table-title">
		<h3>社員情報一覧</h3>
	</div>

	<div class="table-content">
		<table class="table list-table-wp">
			<thead>
				<tr>
					<td><span class="thead-text">氏名</span></td>
					<td><span class="thead-text">性別</span></td>
					<td><span class="thead-text">生年月日</span></td>
					<td><span class="thead-text">年齢</span></td>
					<td><span class="thead-text">経験年数</span></td>
					<td><span class="thead-text">所属部署</span></td>
					<td><span class="thead-text">資格</span></td>
				</tr>
			</thead>
			<c:if test="${listUser != null}">
				<tbody>
					<c:forEach var="item" items="${listUser}">
						<c:choose>
							<c:when test="${item.getStatus() == 0}">
								<tr>
									<td class=""><a href="${item.getDetailUrl()}" title="">${item.getFullname()}</a>
									</td>
									<td>
										<span class="tbody-text">
											<c:choose>
												<c:when test="${item.getGender() == 0}">男</c:when>
												<c:when test="${item.getGender() == 1}">女</c:when>
											</c:choose>
										</span>
										</td>
									<td><span class="tbody-text">${item.getBirth()}</span></td>
									<td><span class="tbody-text">${item.getAge()}</span></td>
									<td><span class="tbody-text">${item.getExperience()}</span></td>
									<td><span class="tbody-text">${item.getDepartmentName()}</span></td>
									<td>
										<c:if test="${item.getListCertificate() != null}">
											<c:forEach var="cer" items="${item.getListCertificate()}">
												<p class="tbody-text">${cer.getCertificateName()}</p>
											</c:forEach>
										</c:if>
									</td>
								</tr>
							</c:when>
						</c:choose>
					</c:forEach>

				</tbody>
			</c:if>
		</table>
	</div>
</div>
<c:choose>
	<c:when  test="${pagging != null}">
		${pagging.getPagging()}
	</c:when>
</c:choose>
<%@include file="/footer.html"%>
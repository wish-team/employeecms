package admin.user;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.UserDAO;
import tool.Action;

public class DeleteAction extends Action {
	public String execute(
			HttpServletRequest request, HttpServletResponse response) throws Exception {

		String id = request.getParameter("id");
		UserDAO dao = new UserDAO();

		String status = "1";
		HashMap<String, String> data = new HashMap<String, String>();
		data.put("status", status);

		HashMap<String, String> where = new HashMap<String, String>();
		where.put("id", id);
		dao.dbUpdate("tbl_user", data, null, where, 0);

		return "../user/ListUser.action";
	}

}

package admin.user;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import bean.Manager;
import bean.User;
import dao.ManagerDAO;
import dao.UserDAO;
import tool.Action;

public class EditAction extends Action{
	public String execute(
			HttpServletRequest request, HttpServletResponse response
	) throws Exception {

		String id =request.getParameter("id");
		UserDAO dao=new UserDAO();

		String btn = request.getParameter("btn-submit");
		if(btn == null) btn ="";

		if(!(btn.isEmpty()) || (btn.equals("submit"))) {
			String fullname = request.getParameter("fullname");
			String department = request.getParameter("department");//
			String birth = request.getParameter("birth");
			String age = request.getParameter("age");
			String gender = request.getParameter("gender");
			String experience = request.getParameter("experience");
			String diplomaOp1 = request.getParameter("diplomaOp1");
			String diplomaOp2 = request.getParameter("diplomaOp2");
			String diplomaOp3 = request.getParameter("diplomaOp3");
			ArrayList<String> listCertificateData = new ArrayList<String>();
			if(!diplomaOp1.isEmpty()) listCertificateData.add(diplomaOp1);
			if(!diplomaOp2.isEmpty()) listCertificateData.add(diplomaOp2);
			if(!diplomaOp3.isEmpty()) listCertificateData.add(diplomaOp3);

			HashMap<String, String> data = new HashMap<String, String>();
			data.put("fullname", fullname);
			if(!department.isEmpty()) data.put("department", department);
			data.put("birth", birth);
			data.put("age", age);
			data.put("sex", gender);
			data.put("experience", experience);

			HashMap<String, String> where = new HashMap<String, String>();
			where.put("id", id);
			dao.dbUpdate("tbl_user", data, null, where, 0);

			HashMap<String, String> whereCerData = new HashMap<String, String>();
			whereCerData.put("userId", id);
			List<User> listCertificate = dao.dbGetData("select * from tbl_certificate_detail as dt INNER JOIN tbl_certificate as c ON dt.certificateId = c.id", whereCerData, "listCertificate");
			HashMap<String, String> whereCer = new HashMap<String, String>();
			HashMap<String, String> dataCer = new HashMap<String, String>();
			int i = 0;
			for(User item: listCertificate) {
				String cerId = Integer.toString(item.getId());
				whereCer.put("userId", id);
				whereCer.put("id", cerId);
				dataCer.put("certificateId", listCertificateData.get(i));
				dao.dbUpdate("tbl_certificate_detail", dataCer, null, whereCer, 0);
				i++;
			}
			return "Detail.action?id" + id;
		}

		HashMap<String, String> whereInfo = new HashMap<String, String>();
		whereInfo.put("us.id",id);
		HashMap<String, String> where = new HashMap<String, String>();
		where.put("userId", id);
		List<User> userInfo = dao.dbGetData("select * from tbl_user as us JOIN tbl_department as dp ON us.department = dp.id ", whereInfo, "user");
		List<User> listCertificate = dao.dbGetData("select * from tbl_certificate_detail as dt INNER JOIN tbl_certificate as c ON dt.certificateId = c.id", where, "listCertificate");
		List<User> listWorkHistory = dao.dbGetData("select * from tbl_work_history as wh join tbl_industry as i on wh.industryId = i.id", where, "workHistory");

		User oneUserInfo = userInfo.get(0);
		oneUserInfo.setEditUserInfoUrl("../user/Edit.action?id=" + id);

		for(User item : listWorkHistory) {
			String workHistoryId = Integer.toString(item.getWorkHistoryId());
			HashMap<String, String> whereWorkHistory = new HashMap<String, String>();
			whereWorkHistory.put("workHistoryId", workHistoryId);

			String startTimeStr = item.getStartTime();
			String[] startTime = startTimeStr.split("-");
			item.setYearStart(startTime[0]);
			item.setMounthStart(startTime[1]);
			item.setDayStart(startTime[2]);

			String[] endTime = (item.getEndTime()).split("-");
			item.setYearEnd(endTime[0]);
			item.setMounthEnd(endTime[1]);
			item.setDayEnd(endTime[2]);

			item.setListLanguage( dao.dbGetData("select languageName, la.id from tbl_work_history_detail as hd join tbl_language as la on hd.languageId = la.id", whereWorkHistory, "listLanguage"));
			item.setListDatabase( dao.dbGetData("select databaseName, db.id from tbl_work_history_detail as hd join tbl_database as db on hd.databaseId = db.id", whereWorkHistory, "listDatabase"));
			item.setListFramework( dao.dbGetData("select frameworkName, fw.id from tbl_work_history_detail as hd join tbl_framework as fw on hd.frameworkId = fw.id", whereWorkHistory, "listFramework"));
			item.setEditUserWorkHistoryUrl("../user/EditUserWorkHistory.action?workHistoryId=" + workHistoryId);
		}

		ManagerDAO masterDao = new ManagerDAO();
		List<Manager> listIndustryDb = masterDao.dbGetData("select * from tbl_industry", null, "industry");
		List<Manager> listLangugeDb = masterDao.dbGetData("select * from tbl_language", null, "language");
		List<Manager> listDatabseDb = masterDao.dbGetData("select * from tbl_database", null, "database");
		List<Manager> listFrameworkDb = masterDao.dbGetData("select * from tbl_framework", null, "framework");
		List<Manager> listDepartmentDb = masterDao.dbGetData("select * from tbl_department", null, "department");
		List<Manager> listCertificateDb = masterDao.dbGetData("select * from tbl_certificate", null, "certificate");

		request.setAttribute("userInfo", userInfo);
		request.setAttribute("listCertificate", listCertificate);
		request.setAttribute("listWorkHistory", listWorkHistory);
		request.setAttribute("listDepartmentDb", listDepartmentDb);
		request.setAttribute("listCertificateDb", listCertificateDb);
		request.setAttribute("listIndustryDb", listIndustryDb);
		request.setAttribute("listLangugeDb", listLangugeDb);
		request.setAttribute("listDatabaseDb", listDatabseDb);
		request.setAttribute("listFrameworkDb", listFrameworkDb);

		return "edit.jsp";
	}


}

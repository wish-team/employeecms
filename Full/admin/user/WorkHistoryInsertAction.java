package admin.user;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import bean.Manager;
import bean.User;
import dao.ManagerDAO;
import dao.UserDAO;
import tool.Action;

public class WorkHistoryInsertAction extends Action {

	public String execute(
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		// step 1 ボタン押すかどうか判定処理
		// step2 ボタンを押さない場合｛
        String userId = request.getParameter("id");
        request.setAttribute("id", userId);
		String btn = request.getParameter("btn-submit");
		if (btn == null) btn = "";

		if ((btn.isEmpty()) || btn==null) {
			String id = request.getParameter("id");
			ManagerDAO dao = new ManagerDAO();
			UserDAO Udao = new UserDAO();

			HashMap<String, String> whereUserInfo = new HashMap<String, String>();
			List<User> userInfo = new ArrayList<User>();
//			List<User> workHistoryInfo = new ArrayList<User>();

			List<Manager> listLanguage = new ArrayList<Manager>();
			List<Manager> listDatabase = new ArrayList<Manager>();
			List<Manager> listFramework = new ArrayList<Manager>();
			List<Manager> listIndustry = new ArrayList<Manager>();

			whereUserInfo.put("us.id", id);
			userInfo = Udao.dbGetData("select * from TBL_USER as us JOIN TBL_DEPARTMENT as dp ON us.department= dp.id ",
					whereUserInfo, "user");
			request.setAttribute("userInfo", userInfo);
			User user = userInfo.get(0);
			String birth = user.getBirth();
			//(1)分割する文字列を生成
			 String str = new String(birth);
			//(2)文字列をsplitメソッドで分割
			 String[] strAry = str.split("-");

			  String year = strAry[0];
			 String month = strAry[1];
			 String day = strAry[2];
			 request.setAttribute("year",year);
			request.setAttribute("month",month);
			request.setAttribute("day",day);

			int gender = user.getGender();

			if(gender==0) {
				String gendergudge = "男";
				request.setAttribute("Gender",gendergudge);
			}else {
				String  gendergudge = "女";
				request.setAttribute("Gender",gendergudge);
			}

			HashMap<String, String> where = new HashMap<String, String>();

			where.put("userid", id);
			//workHistoryInfo = Udao.dbGetData("select * from tbl_work_history ", where, "workhistory");
			//request.setAttribute("workHistoryInfo", workHistoryInfo);

			List<User> listCertificate = Udao.dbGetData(
					"select * from tbl_certificate_detail as dt INNER JOIN tbl_certificate as c ON dt.certificateId = c.id",
					where, "listCertificate");

			int i = 0;
			for (User item : listCertificate) {
				if (i == 0) {
					String certificate1 = item.getCertificateName();
					request.setAttribute("certificate1", certificate1);
				}
				if (i == 1) {
					String certificate2 = item.getCertificateName();
					request.setAttribute("certificate2", certificate2);
				}
				if (i == 2) {
					String certificate3 = item.getCertificateName();
					request.setAttribute("certificate3", certificate3);
				}
				i++;
			}

			request.setAttribute("listCertificate", listCertificate);

			listLanguage = dao.dbGetData("select * from tbl_language", null, "language");
			request.setAttribute("listLanguage", listLanguage);

			listDatabase = dao.dbGetData("select * from tbl_database", null, "database");
			request.setAttribute("listDatabase", listDatabase);

			listFramework = dao.dbGetData("select * from tbl_framework", null, "framework");
			request.setAttribute("listFramework", listFramework);

			listIndustry = dao.dbGetData("select * from tbl_industry", null, "industry");
			request.setAttribute("listIndustry", listIndustry);

			return "../user/workHistoryInsert.jsp";

		}
		// mastar 取得　JSPへ遷移
		//押す場合の追加処理

		//表示する社員のIDを取

		//期間の開始日を取得してyyyy/mm/ddに変換
		String startYear = request.getParameter("StartYear");
		String startMonth = request.getParameter("StartMonth");
		String startDay = request.getParameter("StartDay");
		String startTime = startYear + "-" + startMonth + "-" + startDay;
		//int StartTime = Integer.parseInt(JoinStartTime);
		//期間の終了日を取得してyyyy/mm/ddに変換
		String endYear = request.getParameter("EndYear");
		String endMonth = request.getParameter("EndMonth");
		String endDay = request.getParameter("EndDay");
		String endTime = endYear + "-" + endMonth + "-" + endDay;

		//int EndTime = Integer.parseInt(JoinEndTime);
		//職務経歴の項目を取得
		String industry = request.getParameter("industry");
		String detail = request.getParameter("detail");
		String[] language = request.getParameterValues("language");
		String[] database = request.getParameterValues("database");
		String[] framework = request.getParameterValues("framework");

		//期間に入力漏れがあった場合、エラーを表示
		if (startYear.isEmpty() || startMonth.isEmpty() || startDay.isEmpty()
				|| endYear.isEmpty() || endMonth.isEmpty() || endDay.isEmpty())
			return "エラーのｊｓｐ処理";

		HashMap<String, String> map = new HashMap<String, String>();

		map.put("workhistoryid", null);
		map.put("industryid", industry);
		map.put("userid", userId);
		map.put("detail", detail);
		map.put("starttime", startTime);
		map.put("endtime", endTime);

//		ManagerDAO dao = new ManagerDAO();
		UserDAO Udao = new UserDAO();
		int workHistoryId = Udao.dbInsert("tbl_work_history", map, 1);

//		List<User> workHistoryDetail = new ArrayList<User>();

		HashMap<String, String> mapDetail = new HashMap<String, String>();
		mapDetail.put("id", null);
		mapDetail.put("workHistoryId", Integer.toString(workHistoryId));
		for(int i =0;i<3;i++) {
			if (!language[i].isEmpty()) {
				mapDetail.put("languageId", language[i]);
			}else{
				mapDetail.put("languageId", null);
			}
			if (!database[i].isEmpty()) {
				mapDetail.put("databaseId", database[i]);
			}else{
				mapDetail.put("databaseId", null);
			}
			if (!framework[i].isEmpty()) {
				mapDetail.put("frameworkId", framework[i]);
			}else{
				mapDetail.put("frameworkId", null);
			}
			if(language[i].isEmpty()&&database[i].isEmpty()&&framework[i].isEmpty()) {

			}else
				Udao.dbInsert("tbl_work_history_detail", mapDetail, 1);
			}

		return "Detail.action?id="+userId;

	}
}

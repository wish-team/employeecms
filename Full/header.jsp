<%@page contentType="text/html; charset=UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Wish Team Project</title>
    <link href="/css/style.css" rel="stylesheet" type="text/css">
	<style>
		<%@include file="/css/reset.css"%>
		<%@include file="/css/global.css"%>
		<%@include file="/css/style.css"%>
	</style>
</head>

<body>
    <div id="wrapper">
        <div class="wp-inner">
            <div id="header">
                <div id="mainMenu">
                    <div class="menu-title">
                        <p>社員情報管理システム</p>
                    </div>

                    <div class="menu-content">
                        <a href="../user/AddUserForm.action">社員登録</a>
                        <a href="../user/ListUser.action">社員情報一覧</a>
                        <a href="../search/Search.action">社員情報検索</a>
                        <a href="../manager/Master.action">元帳管理</a>
                        <a href="../manager/AddAdminForm.action">管理者登録</a>
                        <a href="">
                        	<c:choose>
                    			<c:when test="${isLogin == true }">
                    				${username}
                    			</c:when>
                    		</c:choose>
                        </a>
                        <a href="../manager/Logout.action">ログアウト</a>
                    </div>
                </div>
            </div>
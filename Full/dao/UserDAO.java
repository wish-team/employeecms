package dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import bean.Manager;
import bean.User;

public class UserDAO extends DAO {
	public List<User> dbGetData(String query, HashMap<String, String> where, String option) throws Exception {
		List<User> list = new ArrayList<>();

		Connection con = getConnection();
		PreparedStatement st = null;

		if ((where == null) || (where.isEmpty()))
			st = con.prepareStatement(query);
		if ((where != null)) {
			int trigger = 1;
			query += " where ";
			for (String key : where.keySet()) {
				if (trigger == where.size())
					query += key + "=?";
				if (trigger != where.size())
					query += key + "=?,";
				trigger++;
			}
			st = con.prepareStatement(query);
			int i = 1;
			for (String key : where.keySet()) {
				st.setString(i, where.get(key));
				i++;
			}
		}

		ResultSet rs = st.executeQuery();
		if (option.equals("user")) {
			while (rs.next()) {
				User user = new User();
				user.setUserId(rs.getInt("id"));
				user.setFullname(rs.getString("fullname"));
				user.setGender(rs.getInt("sex"));
				user.setBirth(rs.getString("birth"));
				user.setAge(rs.getInt("age"));
				user.setExperience(rs.getInt("experience"));
				user.setDepartmentName(rs.getString("departmentName"));
				user.setStatus(rs.getInt("status"));
				//				user.setCreator(rs.getString("username"));
				//				user.setCreateDate(rs.getString("createDate"));
				//				user.setEditor(rs.getString("editor"));
				//				user.setEditDate(rs.getString("editDate"));
				//				user.setDeleter(rs.getString("deleter"));
				//				user.setDeleteDate(rs.getString("deleteDate"));
				user.setUsername(rs.getString("username"));
				user.setPassword(rs.getString("password"));
				user.setEmployeeEditor(rs.getString("employeeEditor"));
				user.setEmployeeEditDate(rs.getString("employeeEditDate"));

				list.add(user);
			}
		}

		if (option.equals("listCertificate")) {
			while (rs.next()) {
				User user = new User();
				user.setCertificateName(rs.getString("certificateName"));
				user.setId(rs.getInt("id"));
				list.add(user);
			}
		}

		if (option.equals("workHistory")) {
			while (rs.next()) {
				User workHistory = new User();
				workHistory.setWorkHistoryId(rs.getInt("WorkHistoryId"));
				workHistory.setStartTime(rs.getString("startTime"));
				workHistory.setEndTime(rs.getString("endTime"));
				workHistory.setIndustryName(rs.getString("industryName"));
				workHistory.setDetail(rs.getString("detail"));
				list.add(workHistory);
			}
		}

		if (option.equals("workHistoryDetail")) {
			while (rs.next()) {
				User workDetail = new User();
				workDetail.setId(rs.getInt("id"));
				workDetail.setWorkHistoryId(rs.getInt("workHistoryId"));
				workDetail.setLanguageName(rs.getString("languageName"));
				workDetail.setDatabaseName(rs.getString("databaseName"));
				workDetail.setFrameworkName(rs.getString("frameworkName"));
				list.add(workDetail);
			}
		}

		if (option.equals("workHistoryDetailById")) {
			while (rs.next()) {
				User workDetail = new User();
				workDetail.setId(rs.getInt("id"));
				list.add(workDetail);
			}
		}

		if (option.equals("listLanguage")) {
			while (rs.next()) {
				User language = new User();
				language.setLanguageName(rs.getString("languageName"));
				language.setId(rs.getInt("id"));
				list.add(language);
			}
		}

		if (option.equals("listDatabase")) {
			while (rs.next()) {
				User database = new User();
				database.setDatabaseName(rs.getString("databaseName"));
				database.setId(rs.getInt("id"));
				list.add(database);
			}
		}

		if (option.equals("listFramework")) {
			while (rs.next()) {
				User framework = new User();
				framework.setFrameworkName(rs.getString("frameworkName"));
				framework.setIdStr(rs.getString("id"));
				list.add(framework);
			}
		}

		st.close();
		con.close();

		return list;
	}

	public List<User> search(String fullname, String diplomaOp1, String diplomaOp2, String diplomaOp3, String age,
			String exStart, String exEnd) throws Exception {
		List<User> list = new ArrayList<>();
		ArrayList<String> listItem = new ArrayList<String>();
		ArrayList<String> listField = new ArrayList<String>();
		Connection con = getConnection();
		PreparedStatement st = null;
		String query = "select distinct us.id from (TBL_USER as us JOIN TBL_CERTIFICATE_DETAIL as dt on us.id = dt.userid )"
				+ "join tbl_department as dp on us.department = dp.id where ";
		String subQueryFullname = "";
		String subQueryAge = "";
		String subQueryEx = "";
		String subQueryCer = "";

		if (!fullname.isEmpty()) {
			subQueryFullname += "fullname like ?";
			listItem.add(subQueryFullname);
			listField.add(fullname);
		}
		if (!age.isEmpty()) {
			subQueryAge += "( age >= ? AND age <= ? )";
			listItem.add(subQueryAge);
			listField.add(age + "1");
			listField.add(age + "9");
		}
		if (!(exStart.isEmpty()) && (exEnd.isEmpty())) {
			subQueryEx += "experience >= ?";
			listItem.add(subQueryEx);
			listField.add(exStart);
		}
		if ((exStart.isEmpty()) && !(exEnd.isEmpty())) {
			subQueryEx += "experience <= ?";
			listItem.add(subQueryEx);
			listField.add(exEnd);
		}
		if (!(exStart.isEmpty()) && !(exEnd.isEmpty())) {
			subQueryEx += "( experience >= ? AND experience <= ? )";
			listItem.add(subQueryEx);
			listField.add(exStart);
			listField.add(exEnd);
		}

		ArrayList<String> listCertificate = new ArrayList<String>();
		if (!diplomaOp1.isEmpty())
			listCertificate.add(diplomaOp1);
		if (!diplomaOp2.isEmpty())
			listCertificate.add(diplomaOp2);
		if (!diplomaOp3.isEmpty())
			listCertificate.add(diplomaOp3);
		if (listCertificate.size() > 0) {
			subQueryCer += "certificateId IN (";
			String[] listCer = listCertificate.toArray(new String[0]);
			if (listCer.length == 1) {
				subQueryCer += "?)";
				listItem.add(subQueryCer);
				listField.add(listCer[0]);
			}
			if (listCer.length == 2) {
				subQueryCer += "?,?)";
				listItem.add(subQueryCer);
				listField.add(listCer[0]);
				listField.add(listCer[1]);
			}
			if (listCer.length == 3) {
				subQueryCer += "?,?,?)";
				listItem.add(subQueryCer);
				listField.add(listCer[0]);
				listField.add(listCer[1]);
				listField.add(listCer[2]);
			}
		}
		int i = 1;
		for (String item : listItem) {
			if (i != listItem.size() && item != null && !item.isEmpty())
				query += item + " AND ";
			if (i == listItem.size() && item != null && !item.isEmpty())
				query += item;
			i++;
		}
		query += "";
		query += "";
		st = con.prepareStatement(query);

		i = 1;
		for (String key : listField) {
			st.setString(i, key);
			i++;
		}

		ResultSet rs = st.executeQuery();

		while (rs.next()) {
			User user = new User();
			user.setUserId(rs.getInt("id"));
			//			user.setFullname(rs.getString("fullname"));
			//			user.setGender(rs.getInt("sex"));
			//			user.setBirth(rs.getString("birth"));
			//			user.setAge(rs.getInt("age"));
			//			user.setExperience(rs.getInt("experience"));
			//			user.setDepartmentName(rs.getString("departmentName"));
			//			user.setStatus(rs.getInt("status"));
			//			user.setCreator(rs.getString("creator"));
			//			user.setCreateDate(rs.getString("createdate"));
			//			user.setEditor(rs.getString("editor"));
			//			user.setEditDate(rs.getString("editdate"));
			//			user.setDeleter(rs.getString("deleter"));
			//			user.setDeleteDate(rs.getString("deleteDate"));
			//			user.setUsername(rs.getString("username"));
			//			user.setPassword(rs.getString("password"));
			//			user.setEmployeeEditor(rs.getString("employeeEditor"));
			//			user.setEmployeeEditDate(rs.getString("employeeEditDate"));
			list.add(user);
		}

		st.close();
		con.close();

		return list;
	}

	public boolean dbUpdate(String table, HashMap<String, String> data, HashMap<String, Integer> dataInt, HashMap<String, String> where, int option)
			throws Exception {
		Connection con = getConnection();
		String query = "update " + table + " set ";
		PreparedStatement st = null;

		if(option == 1) {
			int trigger = 1;
			for (String key : dataInt.keySet()) {
				if (trigger == dataInt.size()) {
					query += key + "= ?";
				} else {
					query += key + "= ?, ";
				}
				trigger++;
			}

			if (where.size() > 0) {
				trigger = 1;
				query += " where ";
				for (String key : where.keySet()) {
					if (trigger == where.size())
						query += key + "=?";
					if (trigger != where.size())
						query += key + "=? AND ";
					trigger++;
				}
				st = con.prepareStatement(query);
				int i = 1;

				for(String key: dataInt.keySet()) {
					if(dataInt.get(key) == 0) {
						st.setString(i, null);
					}else {
						st.setInt(i, dataInt.get(key));
					}

					i++;
				}

				for (String key : where.keySet()) {
					st.setString(i, where.get(key));
					i++;
				}
			}

			int line = st.executeUpdate();

			if (line != 1) {
				st.close();
				con.close();
				return false;
			}

			st.close();
			con.close();

			return true;
		}

		int trigger = 1;
		for (String key : data.keySet()) {
			if (trigger == data.size()) {
				query += key + "= ?";
			} else {
				query += key + "= ?, ";
			}
			trigger++;
		}

		if (where.size() > 0) {
			trigger = 1;
			query += " where ";
			for (String key : where.keySet()) {
				if (trigger == where.size())
					query += key + "=?";
				if (trigger != where.size())
					query += key + "=? AND ";
				trigger++;
			}
			st = con.prepareStatement(query);
			int i = 1;

			for(String key: data.keySet()) {
				st.setString(i, data.get(key));
				i++;
			}

			for (String key : where.keySet()) {
				st.setString(i, where.get(key));
				i++;
			}
		}

		int line = st.executeUpdate();

		if (line != 1) {
			st.close();
			con.close();
			return false;
		}

		st.close();
		con.close();

		return true;
	}

	public int dbInsert(String table, HashMap<String, String> map, int option) throws Exception {

		String query = "insert into " + table + " (";
		int trigger = 1;
		for (String key : map.keySet()) {
			if (trigger == map.size()) {
				query += key + ") ";
			} else {
				query += key + ",";
			}
			trigger++;
		}
		trigger = 1;
		query += "values (";
		if (option == 1) {
			for (String key : map.keySet()) {
				String value = map.get(key);
				if (trigger == map.size()) {
					if (value == null) {
						query += "null)";
					} else {
						query += "?)";
					}
				} else {
					if (value == null) {
						query += "null,";
					} else {
						query += "? ,";
					}
				}
				trigger++;
			}
		}

		if (option == 2) {
			for (String key : map.keySet()) {
				System.out.println(key);
				if (trigger == map.size()) {
					query += "? )";
				} else {
					query += "?, ";
				}
				trigger++;
			}
		}

		Connection con = getConnection();
		con.setAutoCommit(false);
		PreparedStatement st = con.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
		int i = 1;
		for (String key : map.keySet()) {
			String check = map.get(key);

			if ((check != null)) {
				st.setString(i, map.get(key));
				i++;
			}
		}
		int line = st.executeUpdate();
		ResultSet rs = st.getGeneratedKeys();
		rs.next();
		int primaryKey = -1;
		if (option == 1)
			primaryKey = rs.getInt(1);

		st.close();

		if (line != 1) {
			con.rollback();
			con.setAutoCommit(true);
			con.close();
		}
		con.commit();
		con.setAutoCommit(true);
		con.close();
		return primaryKey;
	}

	/**
	 * Userテーブル、資格テーブルに社員情報を登録する処理
	 * @param user
	 * @return line,line2(段階毎に登録成功したレコード数、失敗なら0が返る
	 * @throws Exception
	 */
	public int insert(User user) throws Exception {


		Connection con = getConnection();

		//オートコミットをオフ
		con.setAutoCommit(false);

		//SQL処理1回目、セッションのLoginIDよりログイン中の管理者のIDを判別
		//SQL文を保持するオブジェクトを生成し、値を代入
		PreparedStatement creator = con.prepareStatement(
				"SELECT ID FROM TBL_ADMIN WHERE USERNAME=?");
		creator.setString(1, user.getCreator());
		ResultSet c_id = creator.executeQuery();

		//ログイン中の管理者のIDを代入
		Manager manager = null;

		while (c_id.next()) {
			manager = new Manager();
			manager.setId(c_id.getInt("id"));

		}

		int creatorid = manager.getId();

		//SQL処理2回目、USERテーブルに値を登録する
		PreparedStatement st = con.prepareStatement(
				"INSERT INTO TBL_USER "
						+ "(ID,FULLNAME,SEX,BIRTH,AGE,EXPERIENCE,DEPARTMENT,STATUS,CREATOR,CREATEDATE,USERNAME,PASSWORD)"
						+ "VALUES (null,?,?,?,?,?,?,?,?,CURRENT_TIMESTAMP,?,?)");
		st.setString(1, user.getFullname());
		st.setInt(2, user.getGender());
		st.setDate(3, (Date) user.getBirth_date());
		st.setInt(4, user.getAge());
		st.setInt(5, user.getExperience());
		st.setInt(6, user.getDepartmentId());	 //参照整合があるので実在しないものを指定するとエラー
		st.setInt(7, 0); 						//削除フラグなので登録時は必ず0
		st.setInt(8, creatorid); 				//参照整合があるので実在しないものを指定するとエラー
		st.setString(9, user.getUsername());
		st.setString(10, user.getPassword());
		int line = st.executeUpdate();

		//登録完了したかの分岐
		if (line > 0) {

			//SQL処理3回目、資格テーブルへ値を登録する為の新規の登録した技術者のIDを判別 //資格テーブル

			//SQL文を保持するオブジェクトを生成し、値を代入
			PreparedStatement u_id;
			u_id = con.prepareStatement(
					"SELECT ID FROM TBL_USER WHERE USERNAME=? AND PASSWORD=?");
			u_id.setString(1, user.getUsername());
			u_id.setString(2, user.getPassword());

			//実行結果を保持するオブジェクトを生成し、値を代入
			ResultSet newid = u_id.executeQuery();


			//実行結果からそれぞれidを取得し、宣言していたusrid変数に入れる

			while (newid.next()) {
				manager = new Manager();
				manager.setId(newid.getInt("id"));
			}

			int Usrid = manager.getId();
			int[] certificate_ids = user.getCertificate_ids();

			//SQL処理4回目、資格テーブルに資格の数の回数登録処理を行う。
			int line2 = 1;
			for (int i = 0; i < certificate_ids.length; i++) {
				PreparedStatement st2 = con.prepareStatement(
						"INSERT INTO TBL_CERTIFICATE_DETAIL (USERID,CERTIFICATEID) VALUES (?,?)");
				st2.setInt(1, Usrid);
				st2.setInt(2, certificate_ids[i]);
				if(certificate_ids[i] != 0) {
					line2 = st2.executeUpdate();
				}

				//条件分岐
				if (line2 == 0) {
					break;
				}
			}

			//すべて更新完了したらオートコミットをオンにしてDB接続を切る
			if (line2 > 0) {
				con.commit();
				con.setAutoCommit(true);
				st.close();
				con.close();

			}else {
				con.rollback();
				con.setAutoCommit(true);
				st.close();
				con.close();
			}

			return line2; //登録失敗の場合line=0 , 成功の場合登録した数を返す

		} else {
			con.rollback();
			con.setAutoCommit(true);
			st.close();
			con.close();

			return line; //登録失敗の場合line=0を返す
		}


	}

	public User login(String username, String password) throws Exception {

		User user = null;

		Connection con = getConnection();

		PreparedStatement st;
		st = con.prepareStatement(
				"select * from tbl_user where username = ? and password = ?");
		st.setString(1, username);
		st.setString(2, password);
		ResultSet rs = st.executeQuery();

		while (rs.next()) {
			user = new User();
			user.setId(rs.getInt("id"));
			user.setFullname(rs.getString("fullname"));
			user.setUsername(rs.getString("username"));
			user.setPassword(rs.getString("password"));
		}

		st.close();
		con.close();
		return user;
	}
}
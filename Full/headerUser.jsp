<%@page contentType="text/html; charset=UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Wish Team Project</title>
    <link href="/css/style.css" rel="stylesheet" type="text/css">
	<style>
		<%@include file="/css/reset.css"%>
		<%@include file="/css/global.css"%>
		<%@include file="/css/style.css"%>
	</style>
</head>

<body>
    <div id="wrapper">
        <div class="wp-inner">
            <div id="header">
                <div id="mainMenu">
                    <div class="menu-title">
                        <p>社員情報管理システム</p>
                    </div>

                    <div class="menu-content">
                        	<c:choose>
                    			<c:when test="${isLogin == true }">
                    				こんにちは! ${username}
                    			</c:when>
                    		</c:choose>
                        </a>
                        <a href="../user/Logout.action">ログアウト</a>
                    </div>
                </div>
            </div>
package admin.manager;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import bean.Error;
import dao.ManagerDAO;
import tool.Action;

public class UpdateDepartmentAction extends Action {
//	@SuppressWarnings("unchecked")
	public String execute(HttpServletRequest request, HttpServletResponse response) throws Exception {

		Error er = new Error();

		int id = Integer.parseInt(request.getParameter("id"));
		String departmentName = request.getParameter("departmentName");
		departmentName = departmentName.trim();

		if (departmentName.isEmpty()) {
			er.setError("部署名を入力してください");
			request.setAttribute("error", er);
			return "editDepartment.jsp";
		}

		ManagerDAO dao=new ManagerDAO();

		/*
		 *部署名・部長の検索と比較
		 */
		if(dao.isExist("tbl_department", "departmentName",departmentName)){
			er.setError("すでに存在している部署名です。");
			request.setAttribute("error", er);
			return "editDepartment.jsp";
		}


		//daoのupdateの処理を呼ぶ
		dao.dbUpdate("tbl_department","departmentname",id, departmentName);

		return "showMaster.jsp";
	}
}

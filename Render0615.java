package bean;

import java.util.List;

public class Render implements java.io.Serializable {

	private String htmlStr;

	//DBのゲッタ
	public String getDatabaseTable() {
		return htmlStr;
	}

	//言語のゲッタ
	public String getLanguageTable() {
		return htmlStr;
	}

	//業種のゲッタ
	public String getIndustryTable() {
		return htmlStr;
	}

	//FWのゲッタ
	public String getFrameworkTable() {
		return htmlStr;
	}

	//資格のゲッタ
	public String getCertificateTable() {
		return htmlStr;
	}

	//部署のゲッタ
	public String getDepartmentTable() {
		return htmlStr;
	}

	//DBのセッタ
	public String setDatabaseTable(List<String> list) {
		this.htmlStr = "<div class='data-table-wp'><div class='data-table-title'><h3>DB一覧</h3><a href='../manager/addDatabase.jsp'>追加</a></div><div class='data-table-content'><table id='master-table' class='table'>";

		//For で繰り返して、データをとる
		this.htmlStr += "<tr class='clearfix'><td><span>DFKDJSHFKJDSHFKJDSHFKJ</span><div class='control-panel fl-right'><a href='????'>変更</a><a href='????'>削除</a></div></td></tr>";

		this.htmlStr += "</table></div></div>";
		return this.htmlStr;
	}

	//言語のセッタ
	public String setLanguageTable(List<String> list) {
		this.htmlStr = "<div class='data-table-wp'><div class='data-table-title'><h3>言語一覧</h3><a href='../manager/addLanguage.jsp'>追加</a></div><div class='data-table-content'><table id='master-table'class='table'>";

		//For で繰り返して、データをとる
		this.htmlStr += "<tr class='clearfix'><td><span>DFKDJSHFKJDSHFKJDSHFKJ</span><div class='control-panel fl-right'><a href='????'>変更</a><a href='????'>削除</a></div></td></tr>";

		this.htmlStr += "</table></div></div>";
		return this.htmlStr;
	}

	//業種のセッタ
	public String setIndustryTable(List<String> list) {
		this.htmlStr = "<div class='data-table-wp'><div class='data-table-title'><h3>業種一覧</h3><a href='../manager/addIndustry.jsp'>追加</a></div><div class='data-table-content'><table id='master-table'class='table'>";

		//For で繰り返して、データをとる
		this.htmlStr += "<tr class='clearfix'><td><span>DFKDJSHFKJDSHFKJDSHFKJ</span><div class='control-panel fl-right'><a href='????'>変更</a><a href='????'>削除</a></div></td></tr>";

		this.htmlStr += "</table></div></div>";
		return this.htmlStr;
	}

	//FWのセッタ
	public String setFrameworkTable(List<String> list) {
		this.htmlStr = "<div class='data-table-wp'><div class='data-table-title'><h3>FW一覧</h3><a href='../manager/addFramework.jsp'>追加</a></div><div class='data-table-content'><table id='master-table'class='table'>";

		//For で繰り返して、データをとる
		this.htmlStr += "<tr class='clearfix'><td><span>DFKDJSHFKJDSHFKJDSHFKJ</span><div class='control-panel fl-right'><a href='????'>変更</a><a href='????'>削除</a></div></td></tr>";

		this.htmlStr += "</table></div></div>";
		return this.htmlStr;
	}

	//資格のセッタ
	public String setCertificateTable(List<String> list) {
		this.htmlStr = "<div class='data-table-wp'><div class='data-table-title'><h3>資格一覧</h3><a href='../manager/addCertificate.jsp'>追加</a></div><div class='data-table-content'><table id='master-table'class='table'>";

		//For で繰り返して、データをとる
		this.htmlStr += "<tr class='clearfix'><td><span>DFKDJSHFKJDSHFKJDSHFKJ</span><div class='control-panel fl-right'><a href='????'>変更</a><a href='????'>削除</a></div></td></tr>";

		this.htmlStr += "</table></div></div>";
		return this.htmlStr;
	}

	//部署のセッタ
	public String setDepartmentTable(List<String> list) {
		this.htmlStr = "<div class='data-table-wp'><div class='data-table-title'><h3>部署一覧</h3><a href='../manager/addDepartment.jsp'>追加</a></div><div class='data-table-content'><table id='master-table'>";

		//For で繰り返して、データをとる
		this.htmlStr += "<tr class='clearfix'><td><span>DFKDJSHFKJDSHFKJDSHFKJ</span><div class='control-panel fl-right'><a href='????'>変更</a><a href='????'>削除</a></div></td></tr>";

		this.htmlStr += "</table></div></div>";
		return this.htmlStr;
	}
}
